// <copyright file="AgentFormTest.cs">Copyright �  2012</copyright>
using System;
using System.Collections.Generic;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using RSS_InsurCompAccountSystem;

namespace RSS_InsurCompAccountSystem
{
    /// <summary>This class contains parameterized unit tests for AgentForm</summary>
    [PexClass(typeof(AgentForm))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class AgentFormTest
    {
        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        public AgentForm Constructor()
        {
            AgentForm target = new AgentForm();
            return target;
            // TODO: add assertions to method AgentFormTest.Constructor()
        }

        /// <summary>Test stub for set_Insurances(List`1&lt;Insurance&gt;)</summary>
        [PexMethod]
        public void InsurancesSet([PexAssumeUnderTest]AgentForm target, List<Insurance> value)
        {
            target.Insurances = value;
            // TODO: add assertions to method AgentFormTest.InsurancesSet(AgentForm, List`1<Insurance>)
        }

        /// <summary>Test stub for set_PersonalInsurances(List`1&lt;Insurance&gt;)</summary>
        [PexMethod]
        public void PersonalInsurancesSet([PexAssumeUnderTest]AgentForm target, List<Insurance> value)
        {
            target.PersonalInsurances = value;
            // TODO: add assertions to method AgentFormTest.PersonalInsurancesSet(AgentForm, List`1<Insurance>)
        }

        /// <summary>Test stub for set_Persons(List`1&lt;Person&gt;)</summary>
        [PexMethod]
        public void PersonsSet([PexAssumeUnderTest]AgentForm target, List<Person> value)
        {
            target.Persons = value;
            // TODO: add assertions to method AgentFormTest.PersonsSet(AgentForm, List`1<Person>)
        }

        /// <summary>Test stub for set_new_rate(Rate)</summary>
        [PexMethod]
        public void new_rateSet([PexAssumeUnderTest]AgentForm target, Rate value)
        {
            target.new_rate = value;
            // TODO: add assertions to method AgentFormTest.new_rateSet(AgentForm, Rate)
        }

        /// <summary>Test stub for set_new_rate_amount(Decimal)</summary>
        [PexMethod]
        public void new_rate_amountSet([PexAssumeUnderTest]AgentForm target, decimal value)
        {
            target.new_rate_amount = value;
            // TODO: add assertions to method AgentFormTest.new_rate_amountSet(AgentForm, Decimal)
        }
    }
}
