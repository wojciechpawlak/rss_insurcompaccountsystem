// <copyright file="FieldValidationTest.cs">Copyright �  2012</copyright>
using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSS_InsurCompAccountSystem;

namespace RSS_InsurCompAccountSystem
{
    /// <summary>This class contains parameterized unit tests for FieldValidation</summary>
    [PexClass(typeof(FieldValidation))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class FieldValidationTest
    {
        /// <summary>Test stub for standardCPR(String)</summary>
        [PexMethod]
        public string standardCPR([PexAssumeUnderTest]FieldValidation target, string input)
        {
            string result = target.standardCPR(input);
            return result;
            // TODO: add assertions to method FieldValidationTest.standardCPR(FieldValidation, String)
        }

        /// <summary>Test stub for standardDate(String)</summary>
        [PexMethod]
        public DateTime standardDate([PexAssumeUnderTest]FieldValidation target, string input)
        {
            DateTime result = target.standardDate(input);
            return result;
            // TODO: add assertions to method FieldValidationTest.standardDate(FieldValidation, String)
        }

        /// <summary>Test stub for standardStringForm(String)</summary>
        [PexMethod]
        public string standardStringForm([PexAssumeUnderTest]FieldValidation target, string input)
        {
            string result = target.standardStringForm(input);
            return result;
            // TODO: add assertions to method FieldValidationTest.standardStringForm(FieldValidation, String)
        }

        /// <summary>Test stub for unicodeNFKC(String)</summary>
        [PexMethod]
        public string unicodeNFKC([PexAssumeUnderTest]FieldValidation target, string input)
        {
            string result = target.unicodeNFKC(input);
            return result;
            // TODO: add assertions to method FieldValidationTest.unicodeNFKC(FieldValidation, String)
        }

        /// <summary>Test stub for validateCPRwithDate(String, DateTime)</summary>
        [PexMethod]
        public bool validateCPRwithDate(
            [PexAssumeUnderTest]FieldValidation target,
            string cpr,
            DateTime dt
        )
        {
            bool result = target.validateCPRwithDate(cpr, dt);
            return result;
            // TODO: add assertions to method FieldValidationTest.validateCPRwithDate(FieldValidation, String, DateTime)
        }
    }
}
