// <copyright file="EmployeeFormTest.cs">Copyright �  2012</copyright>
using System;
using System.Collections.Generic;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using RSS_InsurCompAccountSystem;

namespace RSS_InsurCompAccountSystem
{
    /// <summary>This class contains parameterized unit tests for EmployeeForm</summary>
    [PexClass(typeof(EmployeeForm))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class EmployeeFormTest
    {
        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        public EmployeeForm Constructor()
        {
            EmployeeForm target = new EmployeeForm();
            return target;
            // TODO: add assertions to method EmployeeFormTest.Constructor()
        }

        /// <summary>Test stub for set_InsuranceTypes(List`1&lt;Insurance_Type&gt;)</summary>
        [PexMethod]
        public void InsuranceTypesSet(
            [PexAssumeUnderTest]EmployeeForm target,
            List<Insurance_Type> value
        )
        {
            target.InsuranceTypes = value;
            // TODO: add assertions to method EmployeeFormTest.InsuranceTypesSet(EmployeeForm, List`1<Insurance_Type>)
        }

        /// <summary>Test stub for set_Risks(List`1&lt;Risk&gt;)</summary>
        [PexMethod]
        public void RisksSet([PexAssumeUnderTest]EmployeeForm target, List<Risk> value)
        {
            target.Risks = value;
            // TODO: add assertions to method EmployeeFormTest.RisksSet(EmployeeForm, List`1<Risk>)
        }
    }
}
