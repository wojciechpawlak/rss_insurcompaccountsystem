// <copyright file="IntroFormTest.cs">Copyright �  2012</copyright>
using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSS_InsurCompAccountSystem;

namespace RSS_InsurCompAccountSystem
{
    /// <summary>This class contains parameterized unit tests for IntroForm</summary>
    [PexClass(typeof(IntroForm))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class IntroFormTest
    {
        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        public IntroForm Constructor()
        {
            IntroForm target = new IntroForm();
            return target;
            // TODO: add assertions to method IntroFormTest.Constructor()
        }
    }
}
