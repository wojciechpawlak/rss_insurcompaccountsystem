﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Diagnostics.Contracts;

namespace Model
{
    public class RiskModel : IModel, IRiskModel
    {
        public PersonDataClassesDataContext Data
        {
            get { return new PersonDataClassesDataContext(); }
        }

        public List<Risk> LoadAllRisks()
        {
            Contract.Assert(Data.Risks != null);
            return Data.Risks.ToList();
        }

        public List<Risk> findByInsurance(Insurance insurance)
        {
            Contract.Assert(Data.Risks != null);
            Contract.Assert(insurance != null);
            return Data.Risks.Where(r => r.insurance_type_id == insurance.insurance_type_id).ToList();
        }

        public int getRiskPercentage(Insurance insurance, Risk risk)
        {
            if (insurance == null || risk == null) throw new ArgumentException("Arguments cannot be null");

            Contract.Assert(risk != null);
            Contract.Assert(risk.class_name != null);
            Contract.Assert(risk.field != null);
            Contract.Assert(insurance != null);

            float modelValue = getModelValue(risk.class_name, risk.field, insurance);

            Contract.Assert(modelValue != null);

            if (risk.max != null && risk.min != null)
                return modelValue <= risk.max && modelValue >= risk.min ? risk.value : 0;
            else if (risk.max != null)
                return modelValue <= risk.max ? risk.value : 0;
            else if (risk.min != null)
                return modelValue >= risk.min ? risk.value : 0;
            return 0;
        }

        private float getModelValue(string className, string field, Insurance insurance)
        {
            if (!allowedClasses(className) || !allowedFields(field) || insurance == null) throw new ArgumentException("Invalid className or guid");

            Contract.Assert(className != null);
            Contract.Assert(field != null);
            Contract.Assert(insurance != null);

            Guid   elementGuid    = getProperGuid(className, insurance);

            Contract.Assert(elementGuid != null);

            Object databaseObject = findProperObject(className, elementGuid);

            Contract.Assert(databaseObject != null);

            var result = Type.GetType("Model."+className).InvokeMember(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty, null, databaseObject, null);

            Contract.Assert(result != null);
            
            if (result.GetType() == Type.GetType("System.String"))
            {
                return float.Parse((String)result);
            }
            if (result.GetType() == Type.GetType("System.Int32"))
            {
                int r = (int) result;
                return (float) r;
            }
            return (float)result;
        }

        private Guid getProperGuid(string className, Insurance insurance)
        {
            if (!allowedClasses(className) || insurance == null) throw new ArgumentException("Invalid className or insurance");

            Contract.Assert(className != null);
            Contract.Assert(insurance != null);

            return (Guid)Type.GetType("Model.Insurance").InvokeMember(className.ToLower() + "_id", BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty , null, insurance, null);
        }

        private Object findProperObject(string className, Guid guid)
        {
            if (!allowedClasses(className) || guid == null) throw new ArgumentException("Invalid className or guid");

            Type modelType = Type.GetType("Model."+className + "Model");

            Contract.Assert(modelType != null);

            Object modelObject = (Object)Activator.CreateInstance(modelType);

            Contract.Assert(modelObject != null);

            return modelType.InvokeMember("find", BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod, null, modelObject, new Object[] { guid });
        }

        private bool allowedClasses(string className)   
        {
            if (className == null) throw new ArgumentException("Class name cannot be null");
            string[] classes = new string[] { "Person", "Car", "House" };

            return classes.Contains(className);
        }

        private bool allowedFields(string fieldName)
        {
            if (fieldName == null) throw new ArgumentException("Field name cannor be null");
            string[] classes = new string[] { "birth_day", "birth_place", "cpr", "brand", "engine", "type", "nr_rooms", "post_code"};

            return classes.Contains(fieldName);
        }
    }
}
