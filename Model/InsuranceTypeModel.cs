﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace Model
{
    public class InsuranceTypeModel : IModel, IInsuranceTypeModel
    {
        public PersonDataClassesDataContext Data
        {
            get { return new PersonDataClassesDataContext(); }
        }

        public List<Insurance_Type> LoadAllInsuranceType()
        {
            Contract.Assert(Data.Insurance_Types != null);
            return Data.Insurance_Types.ToList();
        }
    }
}
