﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public interface IRiskModel
    {
        List<Risk> LoadAllRisks();
        List<Risk> findByInsurance(Insurance insurance);
        int getRiskPercentage(Insurance insurance, Risk risk);
    }
}
