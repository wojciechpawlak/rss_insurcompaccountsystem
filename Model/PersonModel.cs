﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics.Contracts;

namespace Model
{
    public class PersonModel : IModel, IPersonModel 
    {
        private PersonDataClassesDataContext _Data;

        public PersonModel()
        {
            _Data = new PersonDataClassesDataContext();
            Contract.Assert(_Data != null);
            Contract.Assert(_Data.Persons != null);
        }

        public PersonDataClassesDataContext Data
        {
            get { return _Data; }
        }
    
        public List<Person> LoadAllPersons()
        {
            return _Data.Persons.ToList();
        }

        public Person find(Guid id)
        {
            Contract.Assert(id != null);

            return _Data.Persons.Where(p => p.person_id == id).First();
        }

        public Person findByCPR(string cpr)
        {
            int i;
	        Contract.Requires(cpr != null);
            Contract.Requires(int.TryParse(cpr, out i));
	        Contract.Requires(cpr.Length == 10);
            Person _p = _Data.Persons.Where(p => p.cpr == cpr).First();
            return _p;
        }

        public bool create(Person p)
        {
            Contract.Assert(p != null);
            try
            {
                System.Guid g;
                do
                {
                    g = System.Guid.NewGuid();
                } while (find(g) != null);
                p.person_id = g;

                Contract.Assert(p.person_id != null);
                _Data.Persons.InsertOnSubmit(p);
                _Data.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Error adding new person " + e.Message);
                return false;
            }
            return true;
        }


        public bool remove(Guid id)
        {
            Contract.Assert(id != null);

            try
            {
                _Data.Persons.DeleteOnSubmit(find(id));
                _Data.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Error removing exisitng person " + e.Message);
                return false;
            }
            return true;
        }
    }
}
