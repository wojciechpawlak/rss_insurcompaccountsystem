﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace Model
{
    public class HouseModel : IModel, IHouseModel
    {

        public PersonDataClassesDataContext Data
        {
            get { return new PersonDataClassesDataContext(); }
        }

        public List<House> LoadAllHouses()
        {
            Contract.Assert(Data.Houses != null);
            return Data.Houses.ToList();
        }

        public House find(Guid id)
        {
            Contract.Assert(id != null);
            Contract.Assert(Data.Houses != null);

            return Data.Houses.Where(h => h.house_id == id).First();
        }
    }
}
