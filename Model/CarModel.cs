﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace Model
{
    public class CarModel
    {
        public PersonDataClassesDataContext Data
        {
            get { return new PersonDataClassesDataContext(); }
        }

        public List<Car> LoadAllCars()
        {
            Contract.Assert(Data.Cars != null);
            return Data.Cars.ToList();
        }

        public Car find(Guid id)
        {
            Contract.Assert(Data.Cars != null);
            Contract.Assert(id != null);
            return Data.Cars.Where(c => c.car_id == id).First();
        }
    }
}
