﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace Model
{
    public class InsuranceModel : IModel, IInsuranceModel
    {
        private PersonDataClassesDataContext _Data;

        public InsuranceModel()
        {
            _Data = new PersonDataClassesDataContext();
            Contract.Assert(_Data != null);
            Contract.Assert(_Data.Insurances != null);
        }

        public PersonDataClassesDataContext Data
        {
            get { return _Data; }
        }

        public List<Insurance> LoadAllInsurances()
        {
            return _Data.Insurances.ToList();
        }

        public List<Insurance> find_by_person(Person person)
        {
            return _Data.Insurances.Where(i => i.person_id == person.person_id).ToList();
        }

        public bool create(Insurance insurance)
        {
            Contract.Assert(insurance != null);
            _Data.Insurances.InsertOnSubmit(insurance);
            _Data.SubmitChanges();
            return true;
        }


        public List<Insurance> find_by_person_id(Guid id)
        {
            Contract.Assert(id != null);
            return _Data.Insurances.Where(i => i.person_id == id).ToList();
        }

        public Insurance find(Guid id)
        {
            Contract.Assert(id != null);
            return _Data.Insurances.Where(i => i.insurance_id == id).First();
        }

        public void save()
        {
            _Data.SubmitChanges();
        }
    }
}
