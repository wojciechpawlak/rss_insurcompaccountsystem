﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace Model
{
    public class RateModel : IModel, IRateModel
    {
        public PersonDataClassesDataContext Data
        {
            get { return new PersonDataClassesDataContext(); }
        }

        public List<Rate> LoadAllRates()
        {
            Contract.Assert(Data.Rates != null);
            return Data.Rates.ToList();
        }

        public Boolean create(Rate r) {
            Contract.Assert(r != null);
            Contract.Assert(Data.Rates != null);
            Contract.Assert(r.rate_id != null);
            Contract.Assert(r.insurance_id != null);
            Contract.Assert(r.amount != null);
            Contract.Assert(r.amount > 0);

            try
            {
                PersonDataClassesDataContext _Data = Data;
                _Data.Rates.InsertOnSubmit(r);
                _Data.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Error adding new rate " + e.Message);
                return false;
            }
            return true;
        }
    }
}
