﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace Model
{
    [ContractClass(typeof(PersonModelContracts))]
    public interface IPersonModel
    {
        List<Person> LoadAllPersons();
        bool create(Person p);
        bool remove(Guid id);
        Person find(Guid id);
        Person findByCPR(string cpr);
    }

    [ContractClassFor(typeof(IPersonModel))]
    internal sealed class PersonModelContracts : IPersonModel
    {
        List<Person> IPersonModel.LoadAllPersons()
        {
            return default(List<Person>);
        }

        bool IPersonModel.create(Person p)
        {
            Contract.Assert(p != null);
            return default(bool);
        }

        bool IPersonModel.remove(Guid guid)
        {
            Contract.Assert(guid != null);
            return default(bool);
        }

        Person IPersonModel.find(Guid id)
        {
            Contract.Assert(id != null);
            return default(Person);
        }

        Person IPersonModel.findByCPR(string cpr)
        {
            Contract.Assert(cpr != null);
            Contract.Assert(cpr.Length == 10);
            return default(Person);
        }

    }
}
