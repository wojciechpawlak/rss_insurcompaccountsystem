﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public interface IInsuranceModel
    {
        List<Insurance> LoadAllInsurances();
        List<Insurance> find_by_person(Person person);
        List<Insurance> find_by_person_id(Guid id);
        Insurance find(Guid id);
        void save();
    }
}
