﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public interface IRateModel
    {
        List<Rate> LoadAllRates();
        Boolean create(Rate r);
    }
}
