﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public interface IInsuranceTypeModel
    {
        List<Insurance_Type> LoadAllInsuranceType();
    }
}
