﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace Presenter
{
    public class RatePresenter
    {
        private RateModel _model;
        private IRateView _view;


        public RatePresenter(IRateView iView)
            : this(iView, new RateModel())
        {

        }

        public RatePresenter(IRateView iView, RateModel model)
        {
            if (iView == null) throw new ArgumentException("View cannot be null");
            _view = iView;
            _model = model;
            WireViewEvents();
        }

        private void WireViewEvents()
        {
            _view.LoadRates += ViewLoadRates;
        }

        private void ViewLoadRates(object sender, EventArgs e)
        {
            _view.Rates = _model.LoadAllRates();
        }
    }
}
