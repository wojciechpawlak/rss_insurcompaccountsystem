﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace Presenter
{
    public class HousePresenter
    {
        private HouseModel _model;
        private IHouseView _view;


        public HousePresenter(IHouseView iView)
            : this(iView, new HouseModel())
        {

        }

        public HousePresenter(IHouseView iView, HouseModel model)
        {
            if (iView == null) throw new ArgumentException("View cannot be null");
            _view = iView;
            _model = model;
            WireViewEvents();
        }

        private void WireViewEvents()
        {
            _view.LoadHouses += ViewLoadHouses;
        }

        private void ViewLoadHouses(object sender, EventArgs e)
        {
            _view.Houses = _model.LoadAllHouses();
        }
    }
}
