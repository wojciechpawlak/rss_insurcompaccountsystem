﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace Presenter
{
    public class InsurancePresenter
    {
        private InsuranceModel _model;
        private IInsuranceView _view;


        public InsurancePresenter(IInsuranceView iView)
            : this(iView, new InsuranceModel())
        {

        }

        public InsurancePresenter(IInsuranceView iView, InsuranceModel model)
        {
            if (iView == null) throw new ArgumentException("View cannot be null");
            _view = iView;
            _model = model;
            WireViewEvents();
        }

        private void WireViewEvents()
        {
            _view.LoadInsurances += ViewLoadInsurances;
            _view.CalculateRate += CalculateNewInsuranceRate;
            _view.LoadPersonalInsurances += ViewLoadPersonInsurance;
        }

        private void ViewLoadInsurances(object sender, EventArgs e)
        {
            _view.Insurances = _model.LoadAllInsurances();
        }

        private void CalculateNewInsuranceRate(object sender, InsuranceEventArgs e)
        {
            Insurance insurance = _model.find((Guid)sender);
            RiskModel riskModel = new RiskModel();
            Decimal risk = 0;
            foreach (Risk r in riskModel.findByInsurance(insurance))
            {
                risk += riskModel.getRiskPercentage(insurance, r);
            }

            Rate rate = new Rate();
            rate.amount = calculateRate((decimal)insurance.Insurance_Type.basic_rate, risk);
            rate.insurance_id = insurance.insurance_id;
            rate.rate_id = Guid.NewGuid();

            RateModel rateModel = new RateModel();
            rateModel.create(rate);
            insurance.last_rate_id = rate.rate_id;
            _model.save();
            _view.new_rate_amount = rate.amount;
        }

        private decimal calculateRate(decimal base_rate, decimal risk)
        {
            if (risk > 1000) throw new ArgumentException("Risk is to high!");
            if (risk < -99) throw new ArgumentException("Risk is to low!");
            if (base_rate < 1) throw new ArgumentException("Base rate is too low!");
            if (base_rate > Decimal.MaxValue / 10) throw new ArgumentException("Base rate is too high!");

            return base_rate * (1 + risk / 100);
        }

        private void ViewLoadPersonInsurance(object sender, EventArgs e)
        {
            Guid id = (Guid)sender;

            _view.PersonalInsurances = _model.find_by_person_id(id);
        }
    }
}
