﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace Presenter
{
    public interface IHouseView
    {
        List<House> Houses { set; }

        event EventHandler LoadHouses;
    }
}
