﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace Presenter
{
    public class PersonPresenter
    {
        private PersonModel _model;
        private IPersonView _view;
        

        public PersonPresenter(IPersonView iView)
            : this(iView, new PersonModel())
        {
            
        }

        public PersonPresenter(IPersonView iView, PersonModel model)
        {
            if (iView == null) throw new ArgumentException("View cannot be null");
            _view = iView;
            _model = model;
            WireViewEvents();
        }

        private void WireViewEvents()
        {
            _view.LoadPersons += ViewLoadPersons;
            _view.AddPerson += ViewAddPerson;
            _view.RemovePerson += ViewRemovePerson;
        }

        private void ViewLoadPersons(object sender, EventArgs e)
        {
            _view.Persons = _model.LoadAllPersons();
        }

        private void ViewAddPerson(object sender, EventArgs e)
        {
            _model.create((Person)sender);
        }

        private void ViewRemovePerson(object sender, EventArgs e)
        {
            _model.remove((Guid)sender);
        }
    }
}