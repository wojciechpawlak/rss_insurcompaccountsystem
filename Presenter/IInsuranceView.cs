﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace Presenter
{
    public interface IInsuranceView
    {
        List<Insurance> Insurances { set; }
        List<Insurance> PersonalInsurances { set; }

        Decimal new_rate_amount { set; }
        Rate new_rate { set; }

        event EventHandler LoadInsurances;
        event EventHandler<InsuranceEventArgs> CalculateRate;
        event EventHandler LoadPersonalInsurances;
    }
}
