﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace Presenter
{
    public interface IPersonView
    {
        List<Person> Persons { set; }

        event EventHandler LoadPersons;
        event EventHandler AddPerson;
        event EventHandler RemovePerson;
    }
}
