﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace Presenter
{
    public class InsuranceTypePresenter
    {
        private InsuranceTypeModel _model;
        private IInsuranceTypeView _view;


        public InsuranceTypePresenter(IInsuranceTypeView iView)
            : this(iView, new InsuranceTypeModel())
        {

        }

        public InsuranceTypePresenter(IInsuranceTypeView iView, InsuranceTypeModel model)
        {
            if (iView == null) throw new ArgumentException("View cannot be null");
            _view = iView;
            _model = model;
            WireViewEvents();
        }

        private void WireViewEvents()
        {
            _view.LoadInsuranceTypes += ViewLoadInsuranceTypes;
        }

        private void ViewLoadInsuranceTypes(object sender, EventArgs e)
        {
            _view.InsuranceTypes = _model.LoadAllInsuranceType();
        }
    }
}
