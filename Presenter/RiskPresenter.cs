﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace Presenter
{
    public class RiskPresenter
    {
        private RiskModel _model;
        private IRiskView _view;


        public RiskPresenter(IRiskView iView)
            : this(iView, new RiskModel())
        {

        }

        public RiskPresenter(IRiskView iView, RiskModel model)
        {
            if (iView == null) throw new ArgumentException("View cannot be null");
            _view = iView;
            _model = model;
            WireViewEvents();
        }

        private void WireViewEvents()
        {
            _view.LoadRisks += ViewLoadRisks;
        }

        private void ViewLoadRisks(object sender, EventArgs e)
        {
            _view.Risks = _model.LoadAllRisks();
        }
    }
}
