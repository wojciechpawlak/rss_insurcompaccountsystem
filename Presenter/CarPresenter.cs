﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace Presenter
{
    public class CarPresenter
    {
        private CarModel _model;
        private ICarView _view;


        public CarPresenter(ICarView iView)
            : this(iView, new CarModel())
        {

        }

        public CarPresenter(ICarView iView, CarModel model)
        {
            if (iView == null) throw new ArgumentException("View cannot be null");
            _view = iView;
            _model = model;
            WireViewEvents();
        }

        private void WireViewEvents()
        {
            _view.LoadCars += ViewLoadCars;
        }

        private void ViewLoadCars(object sender, EventArgs e)
        {
            _view.Cars = _model.LoadAllCars();
        }
    }
}
