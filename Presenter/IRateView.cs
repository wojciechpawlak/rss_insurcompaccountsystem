﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace Presenter
{
    public interface IRateView
    {
        List<Rate> Rates { set; }

        event EventHandler LoadRates;
    }
}
