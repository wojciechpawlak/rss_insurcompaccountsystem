﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace Presenter
{
    public interface IRiskView
    {
        List<Risk> Risks { set; }

        event EventHandler LoadRisks;
    }
}
