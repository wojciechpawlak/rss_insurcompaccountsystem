﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace Presenter
{
    public interface IInsuranceTypeView
    {
        List<Insurance_Type> InsuranceTypes { set; }

        event EventHandler LoadInsuranceTypes;
    }
}
