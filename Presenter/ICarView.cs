﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace Presenter
{
    public interface ICarView
    {
        List<Car> Cars { set; }

        event EventHandler LoadCars;
    }
}
