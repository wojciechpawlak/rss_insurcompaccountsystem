﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace Presenter
{
    public class InsuranceEventArgs : EventArgs
    {
        public InsuranceEventArgs(Insurance value)
        {
            m_insurance = value;
        }

        private Insurance m_insurance;
        public Insurance Value
        {
            get { return m_insurance; }
        }
    }
}
