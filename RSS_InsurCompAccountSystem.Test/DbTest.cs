﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RSS_InsurCompAccountSystem.Test
{
    [TestClass]
    public class DbTest
    {
        [TestMethod]
        public void ShowLocalDbData()
        {
            string mdfFilename = "RSS_InsurCompDB.mdf";
            string outputFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string attachDbFilename = Path.Combine(outputFolder, mdfFilename);
            string connectionString = string.Format(@"Data Source=.\SQLEXPRESS;AttachDbFilename=""{0}"";Integrated Security=True;User Instance=True", attachDbFilename);

            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand("select * from Person", connection);
            SqlDataAdapter adapter = new SqlDataAdapter(command);

            connection.Open();

            int result = command.ExecuteNonQuery();

            Console.WriteLine(result);

            var table = new DataTable();
            int count = adapter.Fill(table);
            Assert.IsTrue(count > 0);

            connection.Close();
        }
    }
}