﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSS_InsurCompAccountSystem;

namespace RSS_InsurCompAccountSystem.Test
{
    [TestClass]
    public class FieldValidationTest
    {
        FieldValidation f;
        [TestInitialize]
        public void setup()
        {
            this.f = new FieldValidation();
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestUnicodeNFKC()
        {
            f.unicodeNFKC(null);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestStandardStringForm1()
        {
            f.standardStringForm(null);
        }
        [TestMethod]
        public void TestStandardStringForm3()
        {
            Assert.AreEqual(f.standardStringForm(@"asd -asd 100 ^ & ( )"),@"asd -asd 100"); 
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestStandardCPR1()
        {
            f.standardCPR(null);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestStandardCPR2()
        {
            f.standardCPR(@"awrongCPR5289045-2852");
        }
        [TestMethod]
        public void TestStandardCPR3()
        {
            Assert.AreEqual(f.standardCPR(@"0209881234"), @"0209881234");
        }
        [TestMethod]
        public void TestStandardCPR4()
        {
            Assert.AreEqual(f.standardCPR(@"0 2 098 8-1  2-34"), @"0209881234");
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestStandardDate1()
        {
            f.standardDate(null);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestStandardDate2()
        {
            f.standardDate(@"12/12/1912");
        }
        [TestMethod]
        public void TestStandardDate3()
        {
            DateTime dt;
            DateTime.TryParse(@"1988-09-02", out dt);
            Assert.AreEqual(f.standardDate(@"1988-09-02"), dt);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void testValidateCPRwithDate1()
        {
            DateTime dt;
            DateTime.TryParse(@"1988-09-02", out dt);
            f.validateCPRwithDate(null, dt);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void testValidateCPRwithDate2()
        {
            DateTime dt;
            DateTime.TryParse(@"1988-09-02", out dt);
            f.validateCPRwithDate(null,dt);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void testValidateCPRwithDate3()
        {
            DateTime dt;
            DateTime.TryParse(@"1923-10-11", out dt);
            Assert.IsTrue(f.validateCPRwithDate(@"0209880987",dt));
        }
    }
}

