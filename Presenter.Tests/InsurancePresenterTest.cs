// <copyright file="InsurancePresenterTest.cs">Copyright �  2012</copyright>
using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using Presenter;

namespace Presenter
{
    /// <summary>This class contains parameterized unit tests for InsurancePresenter</summary>
    [PexClass(typeof(InsurancePresenter))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class InsurancePresenterTest
    {
        /// <summary>Test stub for .ctor(IInsuranceView)</summary>
        [PexMethod]
        public InsurancePresenter Constructor(IInsuranceView iView)
        {
            InsurancePresenter target = new InsurancePresenter(iView);
            return target;
            // TODO: add assertions to method InsurancePresenterTest.Constructor(IInsuranceView)
        }

        /// <summary>Test stub for .ctor(IInsuranceView, InsuranceModel)</summary>
        [PexMethod]
        public InsurancePresenter Constructor01(IInsuranceView iView, InsuranceModel model)
        {
            InsurancePresenter target = new InsurancePresenter(iView, model);
            return target;
            // TODO: add assertions to method InsurancePresenterTest.Constructor01(IInsuranceView, InsuranceModel)
        }
    }
}
