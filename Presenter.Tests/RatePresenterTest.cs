// <copyright file="RatePresenterTest.cs">Copyright �  2012</copyright>
using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using Presenter;

namespace Presenter
{
    /// <summary>This class contains parameterized unit tests for RatePresenter</summary>
    [PexClass(typeof(RatePresenter))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class RatePresenterTest
    {
        /// <summary>Test stub for .ctor(IRateView)</summary>
        [PexMethod]
        public RatePresenter Constructor(IRateView iView)
        {
            RatePresenter target = new RatePresenter(iView);
            return target;
            // TODO: add assertions to method RatePresenterTest.Constructor(IRateView)
        }

        /// <summary>Test stub for .ctor(IRateView, RateModel)</summary>
        [PexMethod]
        public RatePresenter Constructor01(IRateView iView, RateModel model)
        {
            RatePresenter target = new RatePresenter(iView, model);
            return target;
            // TODO: add assertions to method RatePresenterTest.Constructor01(IRateView, RateModel)
        }
    }
}
