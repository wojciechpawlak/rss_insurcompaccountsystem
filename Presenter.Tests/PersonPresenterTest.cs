// <copyright file="PersonPresenterTest.cs">Copyright �  2012</copyright>
using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using Presenter;

namespace Presenter
{
    /// <summary>This class contains parameterized unit tests for PersonPresenter</summary>
    [PexClass(typeof(PersonPresenter))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class PersonPresenterTest
    {
        /// <summary>Test stub for .ctor(IPersonView)</summary>
        [PexMethod]
        public PersonPresenter Constructor(IPersonView iView)
        {
            PersonPresenter target = new PersonPresenter(iView);
            return target;
            // TODO: add assertions to method PersonPresenterTest.Constructor(IPersonView)
        }

        /// <summary>Test stub for .ctor(IPersonView, PersonModel)</summary>
        [PexMethod]
        public PersonPresenter Constructor01(IPersonView iView, PersonModel model)
        {
            PersonPresenter target = new PersonPresenter(iView, model);
            return target;
            // TODO: add assertions to method PersonPresenterTest.Constructor01(IPersonView, PersonModel)
        }
    }
}
