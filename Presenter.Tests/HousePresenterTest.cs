// <copyright file="HousePresenterTest.cs">Copyright �  2012</copyright>
using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using Presenter;

namespace Presenter
{
    /// <summary>This class contains parameterized unit tests for HousePresenter</summary>
    [PexClass(typeof(HousePresenter))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class HousePresenterTest
    {
        /// <summary>Test stub for .ctor(IHouseView)</summary>
        [PexMethod]
        public HousePresenter Constructor(IHouseView iView)
        {
            HousePresenter target = new HousePresenter(iView);
            return target;
            // TODO: add assertions to method HousePresenterTest.Constructor(IHouseView)
        }

        /// <summary>Test stub for .ctor(IHouseView, HouseModel)</summary>
        [PexMethod]
        public HousePresenter Constructor01(IHouseView iView, HouseModel model)
        {
            HousePresenter target = new HousePresenter(iView, model);
            return target;
            // TODO: add assertions to method HousePresenterTest.Constructor01(IHouseView, HouseModel)
        }
    }
}
