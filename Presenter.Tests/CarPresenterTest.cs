// <copyright file="CarPresenterTest.cs">Copyright �  2012</copyright>
using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using Presenter;

namespace Presenter
{
    /// <summary>This class contains parameterized unit tests for CarPresenter</summary>
    [PexClass(typeof(CarPresenter))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class CarPresenterTest
    {
        /// <summary>Test stub for .ctor(ICarView)</summary>
        [PexMethod]
        public CarPresenter Constructor(ICarView iView)
        {
            CarPresenter target = new CarPresenter(iView);
            return target;
            // TODO: add assertions to method CarPresenterTest.Constructor(ICarView)
        }

        /// <summary>Test stub for .ctor(ICarView, CarModel)</summary>
        [PexMethod]
        public CarPresenter Constructor01(ICarView iView, CarModel model)
        {
            CarPresenter target = new CarPresenter(iView, model);
            return target;
            // TODO: add assertions to method CarPresenterTest.Constructor01(ICarView, CarModel)
        }
    }
}
