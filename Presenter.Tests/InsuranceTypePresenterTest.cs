// <copyright file="InsuranceTypePresenterTest.cs">Copyright �  2012</copyright>
using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using Presenter;

namespace Presenter
{
    /// <summary>This class contains parameterized unit tests for InsuranceTypePresenter</summary>
    [PexClass(typeof(InsuranceTypePresenter))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class InsuranceTypePresenterTest
    {
        /// <summary>Test stub for .ctor(IInsuranceTypeView)</summary>
        [PexMethod]
        public InsuranceTypePresenter Constructor(IInsuranceTypeView iView)
        {
            InsuranceTypePresenter target = new InsuranceTypePresenter(iView);
            return target;
            // TODO: add assertions to method InsuranceTypePresenterTest.Constructor(IInsuranceTypeView)
        }

        /// <summary>Test stub for .ctor(IInsuranceTypeView, InsuranceTypeModel)</summary>
        [PexMethod]
        public InsuranceTypePresenter Constructor01(IInsuranceTypeView iView, InsuranceTypeModel model)
        {
            InsuranceTypePresenter target = new InsuranceTypePresenter(iView, model);
            return target;
            // TODO: add assertions to method InsuranceTypePresenterTest.Constructor01(IInsuranceTypeView, InsuranceTypeModel)
        }
    }
}
