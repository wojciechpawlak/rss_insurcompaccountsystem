// <copyright file="RiskPresenterTest.cs">Copyright �  2012</copyright>
using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using Presenter;

namespace Presenter
{
    /// <summary>This class contains parameterized unit tests for RiskPresenter</summary>
    [PexClass(typeof(RiskPresenter))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class RiskPresenterTest
    {
        /// <summary>Test stub for .ctor(IRiskView)</summary>
        [PexMethod]
        public RiskPresenter Constructor(IRiskView iView)
        {
            RiskPresenter target = new RiskPresenter(iView);
            return target;
            // TODO: add assertions to method RiskPresenterTest.Constructor(IRiskView)
        }

        /// <summary>Test stub for .ctor(IRiskView, RiskModel)</summary>
        [PexMethod]
        public RiskPresenter Constructor01(IRiskView iView, RiskModel model)
        {
            RiskPresenter target = new RiskPresenter(iView, model);
            return target;
            // TODO: add assertions to method RiskPresenterTest.Constructor01(IRiskView, RiskModel)
        }
    }
}
