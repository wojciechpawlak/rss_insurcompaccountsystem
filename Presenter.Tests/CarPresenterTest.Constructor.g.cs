// <auto-generated>
// This file contains automatically generated unit tests.
// Do NOT modify this file manually.
// 
// When Pex is invoked again,
// it might remove or update any previously generated unit tests.
// 
// If the contents of this file becomes outdated, e.g. if it does not
// compile anymore, you may delete this file and invoke Pex again.
// </auto-generated>
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Pex.Framework.Generated;
using Presenter.Moles;

namespace Presenter
{
    public partial class CarPresenterTest
    {
[TestMethod]
[PexGeneratedBy(typeof(CarPresenterTest))]
[PexRaisedException(typeof(NullReferenceException))]
public void ConstructorThrowsNullReferenceException295()
{
    CarPresenter carPresenter;
    carPresenter = this.Constructor((ICarView)null);
}
[TestMethod]
[PexGeneratedBy(typeof(CarPresenterTest))]
public void Constructor346()
{
    SICarView sICarView;
    CarPresenter carPresenter;
    sICarView = new SICarView();
    carPresenter = this.Constructor((ICarView)sICarView);
    Assert.IsNotNull((object)carPresenter);
}
    }
}
