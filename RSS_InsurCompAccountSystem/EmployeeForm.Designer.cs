﻿namespace RSS_InsurCompAccountSystem
{
    partial class EmployeeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.employeeTabControl = new System.Windows.Forms.TabControl();
            this.insuranceTypesTabPage = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.insuranceTypesDataGridView = new System.Windows.Forms.DataGridView();
            this.removeInsuranceTypeButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.descriptionITTextBox = new System.Windows.Forms.TextBox();
            this.basicRightITTextBox = new System.Windows.Forms.TextBox();
            this.nameITTextBox = new System.Windows.Forms.TextBox();
            this.editInsuranceTypeButton = new System.Windows.Forms.Button();
            this.addInsuranceTypeButton = new System.Windows.Forms.Button();
            this.risksTabPage = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.risksDataGridView = new System.Windows.Forms.DataGridView();
            this.removeRiskButton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.maxValueRiskTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.minValueRiskTextBox = new System.Windows.Forms.TextBox();
            this.valueRiskTextBox = new System.Windows.Forms.TextBox();
            this.classFieldRiskTextBox = new System.Windows.Forms.TextBox();
            this.classNameRiskTextBox = new System.Windows.Forms.TextBox();
            this.descriptionRiskTextBox = new System.Windows.Forms.TextBox();
            this.editRiskButton = new System.Windows.Forms.Button();
            this.addRiskButton = new System.Windows.Forms.Button();
            this.employeeTabControl.SuspendLayout();
            this.insuranceTypesTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.insuranceTypesDataGridView)).BeginInit();
            this.risksTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.risksDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // employeeTabControl
            // 
            this.employeeTabControl.Controls.Add(this.insuranceTypesTabPage);
            this.employeeTabControl.Controls.Add(this.risksTabPage);
            this.employeeTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.employeeTabControl.Location = new System.Drawing.Point(0, 0);
            this.employeeTabControl.Name = "employeeTabControl";
            this.employeeTabControl.SelectedIndex = 0;
            this.employeeTabControl.Size = new System.Drawing.Size(784, 562);
            this.employeeTabControl.TabIndex = 0;
            // 
            // insuranceTypesTabPage
            // 
            this.insuranceTypesTabPage.Controls.Add(this.splitContainer1);
            this.insuranceTypesTabPage.Location = new System.Drawing.Point(4, 22);
            this.insuranceTypesTabPage.Name = "insuranceTypesTabPage";
            this.insuranceTypesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.insuranceTypesTabPage.Size = new System.Drawing.Size(776, 536);
            this.insuranceTypesTabPage.TabIndex = 0;
            this.insuranceTypesTabPage.Text = "Insurance Type";
            this.insuranceTypesTabPage.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.insuranceTypesDataGridView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.removeInsuranceTypeButton);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.descriptionITTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.basicRightITTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.nameITTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.editInsuranceTypeButton);
            this.splitContainer1.Panel2.Controls.Add(this.addInsuranceTypeButton);
            this.splitContainer1.Size = new System.Drawing.Size(770, 530);
            this.splitContainer1.SplitterDistance = 418;
            this.splitContainer1.TabIndex = 0;
            // 
            // insuranceTypesDataGridView
            // 
            this.insuranceTypesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.insuranceTypesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.insuranceTypesDataGridView.Location = new System.Drawing.Point(0, 0);
            this.insuranceTypesDataGridView.Name = "insuranceTypesDataGridView";
            this.insuranceTypesDataGridView.Size = new System.Drawing.Size(770, 418);
            this.insuranceTypesDataGridView.TabIndex = 0;
            // 
            // removeInsuranceTypeButton
            // 
            this.removeInsuranceTypeButton.Location = new System.Drawing.Point(646, 16);
            this.removeInsuranceTypeButton.Name = "removeInsuranceTypeButton";
            this.removeInsuranceTypeButton.Size = new System.Drawing.Size(100, 23);
            this.removeInsuranceTypeButton.TabIndex = 23;
            this.removeInsuranceTypeButton.Text = "Remove Type";
            this.removeInsuranceTypeButton.UseVisualStyleBackColor = true;
            this.removeInsuranceTypeButton.Click += new System.EventHandler(this.removeInsuranceTypeButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Basic Rate";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Description";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Name";
            // 
            // descriptionITTextBox
            // 
            this.descriptionITTextBox.Location = new System.Drawing.Point(100, 70);
            this.descriptionITTextBox.Name = "descriptionITTextBox";
            this.descriptionITTextBox.Size = new System.Drawing.Size(250, 20);
            this.descriptionITTextBox.TabIndex = 15;
            // 
            // basicRightITTextBox
            // 
            this.basicRightITTextBox.Location = new System.Drawing.Point(100, 44);
            this.basicRightITTextBox.Name = "basicRightITTextBox";
            this.basicRightITTextBox.Size = new System.Drawing.Size(250, 20);
            this.basicRightITTextBox.TabIndex = 14;
            // 
            // nameITTextBox
            // 
            this.nameITTextBox.Location = new System.Drawing.Point(100, 18);
            this.nameITTextBox.Name = "nameITTextBox";
            this.nameITTextBox.Size = new System.Drawing.Size(250, 20);
            this.nameITTextBox.TabIndex = 13;
            // 
            // editInsuranceTypeButton
            // 
            this.editInsuranceTypeButton.Location = new System.Drawing.Point(514, 16);
            this.editInsuranceTypeButton.Name = "editInsuranceTypeButton";
            this.editInsuranceTypeButton.Size = new System.Drawing.Size(100, 23);
            this.editInsuranceTypeButton.TabIndex = 12;
            this.editInsuranceTypeButton.Text = "Edit Type";
            this.editInsuranceTypeButton.UseVisualStyleBackColor = true;
            this.editInsuranceTypeButton.Click += new System.EventHandler(this.editInsuranceTypeButton_Click);
            // 
            // addInsuranceTypeButton
            // 
            this.addInsuranceTypeButton.Location = new System.Drawing.Point(383, 16);
            this.addInsuranceTypeButton.Name = "addInsuranceTypeButton";
            this.addInsuranceTypeButton.Size = new System.Drawing.Size(100, 23);
            this.addInsuranceTypeButton.TabIndex = 11;
            this.addInsuranceTypeButton.Text = "Add New Type";
            this.addInsuranceTypeButton.UseVisualStyleBackColor = true;
            this.addInsuranceTypeButton.Click += new System.EventHandler(this.addInsuranceTypeButton_Click);
            // 
            // risksTabPage
            // 
            this.risksTabPage.Controls.Add(this.splitContainer2);
            this.risksTabPage.Location = new System.Drawing.Point(4, 22);
            this.risksTabPage.Name = "risksTabPage";
            this.risksTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.risksTabPage.Size = new System.Drawing.Size(776, 536);
            this.risksTabPage.TabIndex = 1;
            this.risksTabPage.Text = "Risks";
            this.risksTabPage.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.risksDataGridView);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.removeRiskButton);
            this.splitContainer2.Panel2.Controls.Add(this.label9);
            this.splitContainer2.Panel2.Controls.Add(this.label5);
            this.splitContainer2.Panel2.Controls.Add(this.label4);
            this.splitContainer2.Panel2.Controls.Add(this.label6);
            this.splitContainer2.Panel2.Controls.Add(this.label7);
            this.splitContainer2.Panel2.Controls.Add(this.maxValueRiskTextBox);
            this.splitContainer2.Panel2.Controls.Add(this.label8);
            this.splitContainer2.Panel2.Controls.Add(this.minValueRiskTextBox);
            this.splitContainer2.Panel2.Controls.Add(this.valueRiskTextBox);
            this.splitContainer2.Panel2.Controls.Add(this.classFieldRiskTextBox);
            this.splitContainer2.Panel2.Controls.Add(this.classNameRiskTextBox);
            this.splitContainer2.Panel2.Controls.Add(this.descriptionRiskTextBox);
            this.splitContainer2.Panel2.Controls.Add(this.editRiskButton);
            this.splitContainer2.Panel2.Controls.Add(this.addRiskButton);
            this.splitContainer2.Size = new System.Drawing.Size(770, 530);
            this.splitContainer2.SplitterDistance = 389;
            this.splitContainer2.TabIndex = 0;
            // 
            // risksDataGridView
            // 
            this.risksDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.risksDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.risksDataGridView.Location = new System.Drawing.Point(0, 0);
            this.risksDataGridView.Name = "risksDataGridView";
            this.risksDataGridView.Size = new System.Drawing.Size(770, 389);
            this.risksDataGridView.TabIndex = 0;
            // 
            // removeRiskButton
            // 
            this.removeRiskButton.Location = new System.Drawing.Point(282, 101);
            this.removeRiskButton.Name = "removeRiskButton";
            this.removeRiskButton.Size = new System.Drawing.Size(100, 23);
            this.removeRiskButton.TabIndex = 23;
            this.removeRiskButton.Text = "Remove Risk";
            this.removeRiskButton.UseVisualStyleBackColor = true;
            this.removeRiskButton.Click += new System.EventHandler(this.removeRiskButton_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(391, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Max Value";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(391, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Min Value";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(391, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Value";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Class Field";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Class Name";
            // 
            // maxValueRiskTextBox
            // 
            this.maxValueRiskTextBox.Location = new System.Drawing.Point(467, 68);
            this.maxValueRiskTextBox.Name = "maxValueRiskTextBox";
            this.maxValueRiskTextBox.Size = new System.Drawing.Size(250, 20);
            this.maxValueRiskTextBox.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Description";
            // 
            // minValueRiskTextBox
            // 
            this.minValueRiskTextBox.Location = new System.Drawing.Point(467, 41);
            this.minValueRiskTextBox.Name = "minValueRiskTextBox";
            this.minValueRiskTextBox.Size = new System.Drawing.Size(250, 20);
            this.minValueRiskTextBox.TabIndex = 17;
            // 
            // valueRiskTextBox
            // 
            this.valueRiskTextBox.Location = new System.Drawing.Point(467, 15);
            this.valueRiskTextBox.Name = "valueRiskTextBox";
            this.valueRiskTextBox.Size = new System.Drawing.Size(250, 20);
            this.valueRiskTextBox.TabIndex = 16;
            // 
            // classFieldRiskTextBox
            // 
            this.classFieldRiskTextBox.Location = new System.Drawing.Point(100, 64);
            this.classFieldRiskTextBox.Name = "classFieldRiskTextBox";
            this.classFieldRiskTextBox.Size = new System.Drawing.Size(250, 20);
            this.classFieldRiskTextBox.TabIndex = 15;
            // 
            // classNameRiskTextBox
            // 
            this.classNameRiskTextBox.Location = new System.Drawing.Point(100, 38);
            this.classNameRiskTextBox.Name = "classNameRiskTextBox";
            this.classNameRiskTextBox.Size = new System.Drawing.Size(250, 20);
            this.classNameRiskTextBox.TabIndex = 14;
            // 
            // descriptionRiskTextBox
            // 
            this.descriptionRiskTextBox.Location = new System.Drawing.Point(100, 12);
            this.descriptionRiskTextBox.Name = "descriptionRiskTextBox";
            this.descriptionRiskTextBox.Size = new System.Drawing.Size(250, 20);
            this.descriptionRiskTextBox.TabIndex = 13;
            // 
            // editRiskButton
            // 
            this.editRiskButton.Location = new System.Drawing.Point(152, 101);
            this.editRiskButton.Name = "editRiskButton";
            this.editRiskButton.Size = new System.Drawing.Size(100, 23);
            this.editRiskButton.TabIndex = 12;
            this.editRiskButton.Text = "Edit Risk";
            this.editRiskButton.UseVisualStyleBackColor = true;
            this.editRiskButton.Click += new System.EventHandler(this.editRiskButton_Click);
            // 
            // addRiskButton
            // 
            this.addRiskButton.Location = new System.Drawing.Point(22, 101);
            this.addRiskButton.Name = "addRiskButton";
            this.addRiskButton.Size = new System.Drawing.Size(100, 23);
            this.addRiskButton.TabIndex = 11;
            this.addRiskButton.Text = "Add Risk";
            this.addRiskButton.UseVisualStyleBackColor = true;
            this.addRiskButton.Click += new System.EventHandler(this.addRiskButton_Click);
            // 
            // EmployeeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.employeeTabControl);
            this.Name = "EmployeeForm";
            this.Text = "Employee View";
            this.Load += new System.EventHandler(this.EmployeeForm_Load);
            this.employeeTabControl.ResumeLayout(false);
            this.insuranceTypesTabPage.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.insuranceTypesDataGridView)).EndInit();
            this.risksTabPage.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.risksDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl employeeTabControl;
        private System.Windows.Forms.TabPage insuranceTypesTabPage;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView insuranceTypesDataGridView;
        private System.Windows.Forms.TabPage risksTabPage;
        private System.Windows.Forms.Button removeInsuranceTypeButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox descriptionITTextBox;
        private System.Windows.Forms.TextBox basicRightITTextBox;
        private System.Windows.Forms.TextBox nameITTextBox;
        private System.Windows.Forms.Button editInsuranceTypeButton;
        private System.Windows.Forms.Button addInsuranceTypeButton;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView risksDataGridView;
        private System.Windows.Forms.Button removeRiskButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox minValueRiskTextBox;
        private System.Windows.Forms.TextBox valueRiskTextBox;
        private System.Windows.Forms.TextBox classFieldRiskTextBox;
        private System.Windows.Forms.TextBox classNameRiskTextBox;
        private System.Windows.Forms.TextBox descriptionRiskTextBox;
        private System.Windows.Forms.Button editRiskButton;
        private System.Windows.Forms.Button addRiskButton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox maxValueRiskTextBox;
    }
}