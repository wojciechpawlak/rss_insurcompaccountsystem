﻿namespace RSS_InsurCompAccountSystem
{
    partial class AgentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.personsDataGridView = new System.Windows.Forms.DataGridView();
            this.agentTabControl = new System.Windows.Forms.TabControl();
            this.personsTabPage = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.showReimbursementButton = new System.Windows.Forms.Button();
            this.viewInsurancesButton = new System.Windows.Forms.Button();
            this.removePersonButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.birthPlaceTextBox = new System.Windows.Forms.TextBox();
            this.birthDateTextBox = new System.Windows.Forms.TextBox();
            this.cprTextBox = new System.Windows.Forms.TextBox();
            this.surnameTextBox = new System.Windows.Forms.TextBox();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.editPersonButton = new System.Windows.Forms.Button();
            this.addPersonButton = new System.Windows.Forms.Button();
            this.insurancesTabPage = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.insurancesDataGridView = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.insuranceTypeComboBox = new System.Windows.Forms.ComboBox();
            this.personComboBox = new System.Windows.Forms.ComboBox();
            this.insuranceComboBox = new System.Windows.Forms.ComboBox();
            this.removeInsuranceButton = new System.Windows.Forms.Button();
            this.editInsuranceButton = new System.Windows.Forms.Button();
            this.addInsuranceButton = new System.Windows.Forms.Button();
            this.renewInsurancesTabPage = new System.Windows.Forms.TabPage();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.personsRenewInsurDataGridView = new System.Windows.Forms.DataGridView();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.insurancesRenewInsurDataGridView = new System.Windows.Forms.DataGridView();
            this.newRateLabel = new System.Windows.Forms.Label();
            this.renewInsuranceButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.personsDataGridView)).BeginInit();
            this.agentTabControl.SuspendLayout();
            this.personsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.insurancesTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.insurancesDataGridView)).BeginInit();
            this.renewInsurancesTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.personsRenewInsurDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.insurancesRenewInsurDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // personsDataGridView
            // 
            this.personsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.personsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.personsDataGridView.Location = new System.Drawing.Point(0, 0);
            this.personsDataGridView.Name = "personsDataGridView";
            this.personsDataGridView.Size = new System.Drawing.Size(773, 378);
            this.personsDataGridView.TabIndex = 0;
            // 
            // agentTabControl
            // 
            this.agentTabControl.Controls.Add(this.personsTabPage);
            this.agentTabControl.Controls.Add(this.insurancesTabPage);
            this.agentTabControl.Controls.Add(this.renewInsurancesTabPage);
            this.agentTabControl.Location = new System.Drawing.Point(1, -1);
            this.agentTabControl.Name = "agentTabControl";
            this.agentTabControl.SelectedIndex = 0;
            this.agentTabControl.Size = new System.Drawing.Size(777, 567);
            this.agentTabControl.TabIndex = 1;
            // 
            // personsTabPage
            // 
            this.personsTabPage.Controls.Add(this.splitContainer1);
            this.personsTabPage.Location = new System.Drawing.Point(4, 22);
            this.personsTabPage.Name = "personsTabPage";
            this.personsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.personsTabPage.Size = new System.Drawing.Size(769, 541);
            this.personsTabPage.TabIndex = 0;
            this.personsTabPage.Text = "Persons";
            this.personsTabPage.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(0, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.personsDataGridView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.showReimbursementButton);
            this.splitContainer1.Panel2.Controls.Add(this.viewInsurancesButton);
            this.splitContainer1.Panel2.Controls.Add(this.removePersonButton);
            this.splitContainer1.Panel2.Controls.Add(this.label5);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.birthPlaceTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.birthDateTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.cprTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.surnameTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.firstNameTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.editPersonButton);
            this.splitContainer1.Panel2.Controls.Add(this.addPersonButton);
            this.splitContainer1.Size = new System.Drawing.Size(773, 535);
            this.splitContainer1.SplitterDistance = 378;
            this.splitContainer1.TabIndex = 1;
            // 
            // showReimbursementButton
            // 
            this.showReimbursementButton.Location = new System.Drawing.Point(510, 118);
            this.showReimbursementButton.Name = "showReimbursementButton";
            this.showReimbursementButton.Size = new System.Drawing.Size(205, 23);
            this.showReimbursementButton.TabIndex = 10;
            this.showReimbursementButton.Text = "Show Last Reimbursement to Pay";
            this.showReimbursementButton.UseVisualStyleBackColor = true;
            this.showReimbursementButton.Click += new System.EventHandler(this.showReimbursementButton_Click);
            // 
            // viewInsurancesButton
            // 
            this.viewInsurancesButton.Location = new System.Drawing.Point(379, 118);
            this.viewInsurancesButton.Name = "viewInsurancesButton";
            this.viewInsurancesButton.Size = new System.Drawing.Size(100, 23);
            this.viewInsurancesButton.TabIndex = 9;
            this.viewInsurancesButton.Text = "View Insurances";
            this.viewInsurancesButton.UseVisualStyleBackColor = true;
            this.viewInsurancesButton.Click += new System.EventHandler(this.viewInsurancesButton_Click);
            // 
            // removePersonButton
            // 
            this.removePersonButton.Location = new System.Drawing.Point(642, 14);
            this.removePersonButton.Name = "removePersonButton";
            this.removePersonButton.Size = new System.Drawing.Size(100, 23);
            this.removePersonButton.TabIndex = 8;
            this.removePersonButton.Text = "Remove Person";
            this.removePersonButton.UseVisualStyleBackColor = true;
            this.removePersonButton.Click += new System.EventHandler(this.removePersonButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Birth Place";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Birth Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "CPR";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Surname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "First Name";
            // 
            // birthPlaceTextBox
            // 
            this.birthPlaceTextBox.Location = new System.Drawing.Point(96, 120);
            this.birthPlaceTextBox.Name = "birthPlaceTextBox";
            this.birthPlaceTextBox.Size = new System.Drawing.Size(250, 20);
            this.birthPlaceTextBox.TabIndex = 6;
            // 
            // birthDateTextBox
            // 
            this.birthDateTextBox.Location = new System.Drawing.Point(96, 94);
            this.birthDateTextBox.Name = "birthDateTextBox";
            this.birthDateTextBox.Size = new System.Drawing.Size(250, 20);
            this.birthDateTextBox.TabIndex = 5;
            // 
            // cprTextBox
            // 
            this.cprTextBox.Location = new System.Drawing.Point(96, 68);
            this.cprTextBox.Name = "cprTextBox";
            this.cprTextBox.Size = new System.Drawing.Size(250, 20);
            this.cprTextBox.TabIndex = 4;
            // 
            // surnameTextBox
            // 
            this.surnameTextBox.Location = new System.Drawing.Point(96, 42);
            this.surnameTextBox.Name = "surnameTextBox";
            this.surnameTextBox.Size = new System.Drawing.Size(250, 20);
            this.surnameTextBox.TabIndex = 3;
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Location = new System.Drawing.Point(96, 16);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(250, 20);
            this.firstNameTextBox.TabIndex = 2;
            // 
            // editPersonButton
            // 
            this.editPersonButton.Location = new System.Drawing.Point(510, 14);
            this.editPersonButton.Name = "editPersonButton";
            this.editPersonButton.Size = new System.Drawing.Size(100, 23);
            this.editPersonButton.TabIndex = 1;
            this.editPersonButton.Text = "Edit Person";
            this.editPersonButton.UseVisualStyleBackColor = true;
            this.editPersonButton.Click += new System.EventHandler(this.editPersonButton_Click);
            // 
            // addPersonButton
            // 
            this.addPersonButton.Location = new System.Drawing.Point(379, 14);
            this.addPersonButton.Name = "addPersonButton";
            this.addPersonButton.Size = new System.Drawing.Size(100, 23);
            this.addPersonButton.TabIndex = 0;
            this.addPersonButton.Text = "Add New Person";
            this.addPersonButton.UseVisualStyleBackColor = true;
            this.addPersonButton.Click += new System.EventHandler(this.addPersonButton_Click);
            // 
            // insurancesTabPage
            // 
            this.insurancesTabPage.Controls.Add(this.splitContainer2);
            this.insurancesTabPage.Location = new System.Drawing.Point(4, 22);
            this.insurancesTabPage.Name = "insurancesTabPage";
            this.insurancesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.insurancesTabPage.Size = new System.Drawing.Size(769, 541);
            this.insurancesTabPage.TabIndex = 1;
            this.insurancesTabPage.Text = "Insurances";
            this.insurancesTabPage.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.insurancesDataGridView);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.label6);
            this.splitContainer2.Panel2.Controls.Add(this.insuranceTypeComboBox);
            this.splitContainer2.Panel2.Controls.Add(this.personComboBox);
            this.splitContainer2.Panel2.Controls.Add(this.insuranceComboBox);
            this.splitContainer2.Panel2.Controls.Add(this.removeInsuranceButton);
            this.splitContainer2.Panel2.Controls.Add(this.editInsuranceButton);
            this.splitContainer2.Panel2.Controls.Add(this.addInsuranceButton);
            this.splitContainer2.Panel2.Cursor = System.Windows.Forms.Cursors.Default;
            this.splitContainer2.Size = new System.Drawing.Size(763, 535);
            this.splitContainer2.SplitterDistance = 420;
            this.splitContainer2.TabIndex = 0;
            // 
            // insurancesDataGridView
            // 
            this.insurancesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.insurancesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.insurancesDataGridView.Location = new System.Drawing.Point(0, 0);
            this.insurancesDataGridView.Name = "insurancesDataGridView";
            this.insurancesDataGridView.Size = new System.Drawing.Size(763, 420);
            this.insurancesDataGridView.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(29, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 31;
            // 
            // insuranceTypeComboBox
            // 
            this.insuranceTypeComboBox.FormattingEnabled = true;
            this.insuranceTypeComboBox.Location = new System.Drawing.Point(29, 74);
            this.insuranceTypeComboBox.Name = "insuranceTypeComboBox";
            this.insuranceTypeComboBox.Size = new System.Drawing.Size(250, 21);
            this.insuranceTypeComboBox.TabIndex = 28;
            this.insuranceTypeComboBox.Text = "Insurance Type";
            this.insuranceTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.insuranceTypeComboBox_SelectedIndexChanged);
            // 
            // personComboBox
            // 
            this.personComboBox.FormattingEnabled = true;
            this.personComboBox.Location = new System.Drawing.Point(29, 46);
            this.personComboBox.Name = "personComboBox";
            this.personComboBox.Size = new System.Drawing.Size(250, 21);
            this.personComboBox.TabIndex = 27;
            this.personComboBox.Text = "Person";
            this.personComboBox.SelectedIndexChanged += new System.EventHandler(this.personComboBox_SelectedIndexChanged);
            // 
            // insuranceComboBox
            // 
            this.insuranceComboBox.FormattingEnabled = true;
            this.insuranceComboBox.Location = new System.Drawing.Point(29, 18);
            this.insuranceComboBox.Name = "insuranceComboBox";
            this.insuranceComboBox.Size = new System.Drawing.Size(250, 21);
            this.insuranceComboBox.TabIndex = 26;
            this.insuranceComboBox.Text = "Insurance";
            this.insuranceComboBox.SelectedIndexChanged += new System.EventHandler(this.insuranceComboBox_SelectedIndexChanged);
            // 
            // removeInsuranceButton
            // 
            this.removeInsuranceButton.Location = new System.Drawing.Point(597, 18);
            this.removeInsuranceButton.Name = "removeInsuranceButton";
            this.removeInsuranceButton.Size = new System.Drawing.Size(150, 23);
            this.removeInsuranceButton.TabIndex = 23;
            this.removeInsuranceButton.Text = "Remove Insurance";
            this.removeInsuranceButton.UseVisualStyleBackColor = true;
            this.removeInsuranceButton.Click += new System.EventHandler(this.removeInsuranceButton_Click);
            // 
            // editInsuranceButton
            // 
            this.editInsuranceButton.Location = new System.Drawing.Point(475, 18);
            this.editInsuranceButton.Name = "editInsuranceButton";
            this.editInsuranceButton.Size = new System.Drawing.Size(100, 23);
            this.editInsuranceButton.TabIndex = 12;
            this.editInsuranceButton.Text = "Edit Insurance";
            this.editInsuranceButton.UseVisualStyleBackColor = true;
            this.editInsuranceButton.Click += new System.EventHandler(this.editInsuranceButton_Click);
            // 
            // addInsuranceButton
            // 
            this.addInsuranceButton.Location = new System.Drawing.Point(303, 18);
            this.addInsuranceButton.Name = "addInsuranceButton";
            this.addInsuranceButton.Size = new System.Drawing.Size(150, 23);
            this.addInsuranceButton.TabIndex = 11;
            this.addInsuranceButton.Text = "Add New Insurance";
            this.addInsuranceButton.UseVisualStyleBackColor = true;
            this.addInsuranceButton.Click += new System.EventHandler(this.addInsuranceButton_Click);
            // 
            // renewInsurancesTabPage
            // 
            this.renewInsurancesTabPage.Controls.Add(this.splitContainer3);
            this.renewInsurancesTabPage.Location = new System.Drawing.Point(4, 22);
            this.renewInsurancesTabPage.Name = "renewInsurancesTabPage";
            this.renewInsurancesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.renewInsurancesTabPage.Size = new System.Drawing.Size(769, 541);
            this.renewInsurancesTabPage.TabIndex = 2;
            this.renewInsurancesTabPage.Text = "Renew Insurances";
            this.renewInsurancesTabPage.UseVisualStyleBackColor = true;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(3, 3);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.personsRenewInsurDataGridView);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer3.Size = new System.Drawing.Size(763, 535);
            this.splitContainer3.SplitterDistance = 315;
            this.splitContainer3.TabIndex = 0;
            // 
            // personsRenewInsurDataGridView
            // 
            this.personsRenewInsurDataGridView.AllowUserToAddRows = false;
            this.personsRenewInsurDataGridView.AllowUserToDeleteRows = false;
            this.personsRenewInsurDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.personsRenewInsurDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.personsRenewInsurDataGridView.Location = new System.Drawing.Point(0, 0);
            this.personsRenewInsurDataGridView.Name = "personsRenewInsurDataGridView";
            this.personsRenewInsurDataGridView.ReadOnly = true;
            this.personsRenewInsurDataGridView.Size = new System.Drawing.Size(315, 535);
            this.personsRenewInsurDataGridView.TabIndex = 1;
            this.personsRenewInsurDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.personsRenewInsurDataGridView_CellContentClick);
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.insurancesRenewInsurDataGridView);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.newRateLabel);
            this.splitContainer4.Panel2.Controls.Add(this.renewInsuranceButton);
            this.splitContainer4.Size = new System.Drawing.Size(444, 535);
            this.splitContainer4.SplitterDistance = 494;
            this.splitContainer4.TabIndex = 0;
            // 
            // insurancesRenewInsurDataGridView
            // 
            this.insurancesRenewInsurDataGridView.AllowUserToAddRows = false;
            this.insurancesRenewInsurDataGridView.AllowUserToDeleteRows = false;
            this.insurancesRenewInsurDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.insurancesRenewInsurDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.insurancesRenewInsurDataGridView.Location = new System.Drawing.Point(0, 0);
            this.insurancesRenewInsurDataGridView.Name = "insurancesRenewInsurDataGridView";
            this.insurancesRenewInsurDataGridView.ReadOnly = true;
            this.insurancesRenewInsurDataGridView.Size = new System.Drawing.Size(444, 494);
            this.insurancesRenewInsurDataGridView.TabIndex = 1;
            // 
            // newRateLabel
            // 
            this.newRateLabel.AutoSize = true;
            this.newRateLabel.Location = new System.Drawing.Point(15, 11);
            this.newRateLabel.Name = "newRateLabel";
            this.newRateLabel.Size = new System.Drawing.Size(55, 13);
            this.newRateLabel.TabIndex = 4;
            this.newRateLabel.Text = "New Rate";
            // 
            // renewInsuranceButton
            // 
            this.renewInsuranceButton.Location = new System.Drawing.Point(280, 5);
            this.renewInsuranceButton.Name = "renewInsuranceButton";
            this.renewInsuranceButton.Size = new System.Drawing.Size(150, 23);
            this.renewInsuranceButton.TabIndex = 3;
            this.renewInsuranceButton.Text = "Renew Insurance";
            this.renewInsuranceButton.UseVisualStyleBackColor = true;
            this.renewInsuranceButton.Click += new System.EventHandler(this.renewInsuranceButton_Click);
            // 
            // AgentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.agentTabControl);
            this.Name = "AgentForm";
            this.Text = "Agent View";
            this.Load += new System.EventHandler(this.AgentForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.personsDataGridView)).EndInit();
            this.agentTabControl.ResumeLayout(false);
            this.personsTabPage.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.insurancesTabPage.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.insurancesDataGridView)).EndInit();
            this.renewInsurancesTabPage.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.personsRenewInsurDataGridView)).EndInit();
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.insurancesRenewInsurDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView personsDataGridView;
        private System.Windows.Forms.TabControl agentTabControl;
        private System.Windows.Forms.TabPage personsTabPage;
        private System.Windows.Forms.TabPage insurancesTabPage;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox birthPlaceTextBox;
        private System.Windows.Forms.TextBox birthDateTextBox;
        private System.Windows.Forms.TextBox cprTextBox;
        private System.Windows.Forms.TextBox surnameTextBox;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.Button editPersonButton;
        private System.Windows.Forms.Button addPersonButton;
        private System.Windows.Forms.Button showReimbursementButton;
        private System.Windows.Forms.Button viewInsurancesButton;
        private System.Windows.Forms.Button removePersonButton;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView insurancesDataGridView;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox insuranceTypeComboBox;
        private System.Windows.Forms.ComboBox personComboBox;
        private System.Windows.Forms.ComboBox insuranceComboBox;
        private System.Windows.Forms.Button removeInsuranceButton;
        private System.Windows.Forms.Button editInsuranceButton;
        private System.Windows.Forms.Button addInsuranceButton;
        private System.Windows.Forms.TabPage renewInsurancesTabPage;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.DataGridView personsRenewInsurDataGridView;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.DataGridView insurancesRenewInsurDataGridView;
        private System.Windows.Forms.Label newRateLabel;
        private System.Windows.Forms.Button renewInsuranceButton;
    }
}

