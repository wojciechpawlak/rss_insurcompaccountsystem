﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics.Contracts;

namespace RSS_InsurCompAccountSystem
{
    public class FieldValidation
    {
        public string unicodeNFKC(string input)
        {
            if (input == null)
                throw new ArgumentNullException();
            if (input.Length == 0)
                throw new ArgumentException("Fields cannot be null");
            return input.Normalize(NormalizationForm.FormKC);
        }

        public string standardStringForm(string input)
        {
            if (input == null)
                throw new ArgumentNullException();
            if (input.Length == 0)
                throw new ArgumentException("Fields cannot be null");
            //unicode normalize
            input = input.Normalize(NormalizationForm.FormKC);
            if (input.Length > 50)
                throw new ArgumentException("Data cannot be longer than 50 characters, given: "+input.ToString());
            //remove non-words
            input = Regex.Replace(input, @"[^\w -]", "");
            //remove spaces from beginning/end
            input = input.Trim();
            //remove double spaces
            while (input.Contains("  "))
                input = input.Replace("  ", " ");

            Contract.Assert(input != null);
            Contract.Assert(input.Length > 0);
            return input;
        }

        public string standardCPR(string input)
        {
            if (input == null)
                throw new ArgumentNullException();
            //trim spaces, colons etc
            input = Regex.Replace(input, @"[^\d]", "");
            //check length
            if (input.Length != 10)
                throw new ArgumentException("CPR number must have 10 digits, given: "+input.ToString());
            //validate CPR format
            if (!Regex.IsMatch(input, @"^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])\d\d\d\d\d\d$"))
                throw new ArgumentException("CPR must be valid (wrong birth date), given: "+input.ToString());

            Contract.Assert(input != null);
            Contract.Assert(input.Length == 10);
            Contract.Assert(Regex.IsMatch(input, @"^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])\d\d\d\d\d\d$"));
            return input;
        }

        public DateTime standardDate(string input)
        {
            if (input == null)
                throw new ArgumentNullException("No date provided");
            if (input.Length == 0)
                throw new ArgumentException("Please provide date");
            //validate date format DD-MM-YYYY or DD/MM/YYYY or DD MM YYYY
            if (!Regex.IsMatch(input, @"^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"))
                throw new ArgumentException("Proper date format is DD-MM-YYYY or DD/MM/YYYY or DD MM YYYY, given: " + input.ToString());
            DateTime dt;
            if (!DateTime.TryParse(input, out dt)) //parsing should not fail because of the regexp validation above
                throw new ArgumentException("Proper date format is DD-MM-YYYY or DD/MM/YYYY or DD MM YYYY, given: "+ input.ToString());
            //reasonable year only
            if (dt.Year < 1900)
                throw new ArgumentException("Provide a valid birth date");

            Contract.Assert(input != null);
            Contract.Assert(input.Length == 10);
            Contract.Assert(Regex.IsMatch(input, @"^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"));
            Contract.Assert(dt.Year > 1900);
            return dt;
        }

        public bool validateCPRwithDate(string cpr, DateTime dt)
        {
            if (cpr == null)
                throw new ArgumentNullException("No CPR provided");
            if (cpr.Length != 10)
                throw new ArgumentException("Please provide a valid, 10 digit CPR.");
            //make first 6 digits of CPR from given date
            string cprFromDate = dt.Year.ToString().Substring(2);
            cprFromDate += dt.Month.ToString();
            cprFromDate += dt.Day.ToString();
            //add last 4 digits, now CPR should be equal to cprFromDate if the date in CPR is proper.
            cprFromDate += cpr.Substring(6);
            if (cpr.Equals(cprFromDate))
                return true;
            //otherwise
            throw new ArgumentException("Please provide a valid, 10 digit CPR accordingly to the given birth date: " + cprFromDate.Substring(0,6));
        }
    }
}
