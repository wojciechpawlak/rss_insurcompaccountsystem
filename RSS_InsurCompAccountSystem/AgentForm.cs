﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Model;
using Presenter;
using System.Diagnostics.Contracts;

namespace RSS_InsurCompAccountSystem
{
    public partial class AgentForm : Form, IPersonView, IInsuranceView
    {
        BindingSource personsBindingSource = new BindingSource();
        BindingSource insurancesBindingSource = new BindingSource();
        BindingSource personalInsurancesBindingSource = new BindingSource();

        public AgentForm()
        {
            InitializeComponent();

            new PersonPresenter(this);
            new InsurancePresenter(this);

            personsDataGridView.DataSource = personsBindingSource;
            insurancesDataGridView.DataSource = insurancesBindingSource;

            personsRenewInsurDataGridView.DataSource = personsBindingSource;
            insurancesRenewInsurDataGridView.DataSource = personalInsurancesBindingSource;


        }

        private void AgentForm_Load(object sender, EventArgs e)
        {
            if (LoadPersons != null)
            {
                LoadPersons(sender, e);
            }

            if (LoadInsurances != null)
            {
                LoadInsurances(sender, e);
            }

        }

        protected override void OnClosed(EventArgs e)
        {
            IntroForm introForm = new IntroForm();
            introForm.Show();

            base.OnClosed(e);
        }

        public event EventHandler LoadPersons;
        public event EventHandler AddPerson;
        public event EventHandler RemovePerson;


        public event EventHandler LoadInsurances;
        public event EventHandler LoadPersonalInsurances;
        public event EventHandler<InsuranceEventArgs> CalculateRate;


        
        public List<Person> Persons
        {
            set { personsBindingSource.DataSource = value; }
        }

        public List<Insurance> Insurances
        {
            set { insurancesBindingSource.DataSource = value; }
        }

        public List<Insurance> PersonalInsurances
        {
            set { personalInsurancesBindingSource.DataSource = value; }
        }

        public Rate new_rate
        {
            set { throw new NotImplementedException(); }
        }

        public decimal new_rate_amount
        {
            set { newRateLabel.Text = "New rate: " + value.ToString() +"dkk"; }
        }


        private void addPersonButton_Click(object sender, EventArgs e)
        {
            //make new person from fields
            try
            {
                FieldValidation f = new FieldValidation();
                Person newPerson = new Person();
                newPerson.birth_date = f.standardDate(birthDateTextBox.Text);
                newPerson.birth_place = f.standardStringForm(f.unicodeNFKC(birthPlaceTextBox.Text));
                newPerson.cpr = f.standardCPR(cprTextBox.Text);
                newPerson.first_name = f.standardStringForm(f.unicodeNFKC(firstNameTextBox.Text));
                newPerson.surname = f.standardStringForm(f.unicodeNFKC(surnameTextBox.Text));
                //validate CPR with birth date
                f.validateCPRwithDate(newPerson.cpr,newPerson.birth_date);

                //contracts on person data validation
                Contract.Assert(newPerson != null);
                Contract.Assert(newPerson.birth_date != null);
                Contract.Assert(newPerson.birth_place != null);
                Contract.Assert(newPerson.cpr != null);
                Contract.Assert(newPerson.first_name != null);
                Contract.Assert(newPerson.surname != null);
                
                Contract.Assert(newPerson.birth_date.Year >= 1900);
                Contract.Assert(newPerson.birth_place.Length > 0);
                Contract.Assert(f.validateCPRwithDate(newPerson.cpr, newPerson.birth_date));
                Contract.Assert(newPerson.first_name.Length > 0);
                Contract.Assert(newPerson.surname.Length > 0);

                //add person
                AddPerson(newPerson, null);
                System.Windows.Forms.MessageBox.Show("Adding " + newPerson.first_name + " " + newPerson.surname + ".");
                LoadPersons(sender, e);

                //reset textboxes
                firstNameTextBox.Text = "";
                surnameTextBox.Text = "";
                cprTextBox.Text = "";
                birthDateTextBox.Text = "";
                birthPlaceTextBox.Text = "";
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    System.Windows.Forms.MessageBox.Show("bad input format: " + ex.Message);
                else if (ex is ArgumentNullException)
                    System.Windows.Forms.MessageBox.Show("empty fields not allowed");
                else
                    throw;
            }
        }

        private void editPersonButton_Click(object sender, EventArgs e)
        {

        }

        private void removePersonButton_Click(object sender, EventArgs e)
        {
            Guid id = new Guid(personsDataGridView.CurrentRow.Cells[0].Value.ToString());
            personsDataGridView.Rows.Remove(personsDataGridView.CurrentRow);

            RemovePerson(id, null);
            System.Windows.Forms.MessageBox.Show("Removing " + id + ".");
        }

        private void viewInsurancesButton_Click(object sender, EventArgs e)
        {

        }

        private void showReimbursementButton_Click(object sender, EventArgs e)
        {

        }

        private void addInsuranceButton_Click(object sender, EventArgs e)
        {

        }

        private void editInsuranceButton_Click(object sender, EventArgs e)
        {

        }

        private void removeInsuranceButton_Click(object sender, EventArgs e)
        {

        }

        private void insuranceComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void personComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void insuranceTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void removeInsuranceTypeButton_Click(object sender, EventArgs e)
        {

        }

        private void editInsuranceTypeButton_Click(object sender, EventArgs e)
        {

        }

        private void addInsuranceTypeButton_Click(object sender, EventArgs e)
        {

        }

        private void removeRiskButton_Click(object sender, EventArgs e)
        {

        }

        private void editRiskButton_Click(object sender, EventArgs e)
        {

        }

        private void addRiskButton_Click(object sender, EventArgs e)
        {

        }

        private void renewInsuranceButton_Click(object sender, EventArgs e)
        {
            Guid id = new Guid(insurancesRenewInsurDataGridView.CurrentRow.Cells[0].Value.ToString());
            CalculateRate(id, null);
            Guid person_id = new Guid(personsRenewInsurDataGridView.CurrentRow.Cells[0].Value.ToString());
            LoadPersonalInsurances(person_id, null);
        }

        private void personsRenewInsurDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Guid id = new Guid(personsRenewInsurDataGridView.CurrentRow.Cells[0].Value.ToString());
            LoadPersonalInsurances(id, null);

        }

    }
}