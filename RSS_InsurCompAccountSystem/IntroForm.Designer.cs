﻿namespace RSS_InsurCompAccountSystem
{
    partial class IntroForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.agentButton = new System.Windows.Forms.Button();
            this.employeeButton = new System.Windows.Forms.Button();
            this.introLabel = new System.Windows.Forms.Label();
            this.introLabel2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // agentButton
            // 
            this.agentButton.Location = new System.Drawing.Point(5, 50);
            this.agentButton.Name = "agentButton";
            this.agentButton.Size = new System.Drawing.Size(100, 23);
            this.agentButton.TabIndex = 0;
            this.agentButton.Text = "Agent";
            this.agentButton.UseVisualStyleBackColor = true;
            this.agentButton.Click += new System.EventHandler(this.agentButton_Click);
            // 
            // employeeButton
            // 
            this.employeeButton.Location = new System.Drawing.Point(135, 50);
            this.employeeButton.Name = "employeeButton";
            this.employeeButton.Size = new System.Drawing.Size(100, 23);
            this.employeeButton.TabIndex = 1;
            this.employeeButton.Text = "Employee";
            this.employeeButton.UseVisualStyleBackColor = true;
            this.employeeButton.Click += new System.EventHandler(this.employeeButton_Click);
            // 
            // introLabel
            // 
            this.introLabel.AutoSize = true;
            this.introLabel.Location = new System.Drawing.Point(5, 5);
            this.introLabel.Name = "introLabel";
            this.introLabel.Size = new System.Drawing.Size(232, 13);
            this.introLabel.TabIndex = 2;
            this.introLabel.Text = "Robust Insurance Company Accounting System";
            // 
            // introLabel2
            // 
            this.introLabel2.AutoSize = true;
            this.introLabel2.Location = new System.Drawing.Point(5, 30);
            this.introLabel2.Name = "introLabel2";
            this.introLabel2.Size = new System.Drawing.Size(90, 13);
            this.introLabel2.TabIndex = 3;
            this.introLabel2.Text = "Choose interface:";
            // 
            // IntroForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(239, 82);
            this.Controls.Add(this.introLabel2);
            this.Controls.Add(this.introLabel);
            this.Controls.Add(this.employeeButton);
            this.Controls.Add(this.agentButton);
            this.Name = "IntroForm";
            this.ShowIcon = false;
            this.Text = "RSS_InsurCompAccountSystem Welcome";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button agentButton;
        private System.Windows.Forms.Button employeeButton;
        private System.Windows.Forms.Label introLabel;
        private System.Windows.Forms.Label introLabel2;
    }
}