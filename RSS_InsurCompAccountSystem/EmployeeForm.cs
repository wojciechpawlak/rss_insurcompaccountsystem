﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Model;
using Presenter;

namespace RSS_InsurCompAccountSystem
{
    public partial class EmployeeForm : Form, IInsuranceTypeView, IRiskView
    {
        public EmployeeForm()
        {
            InitializeComponent();

            new InsuranceTypePresenter(this);
            new RiskPresenter(this);
        }

        private void EmployeeForm_Load(object sender, EventArgs e)
        {
            if (LoadInsuranceTypes != null)
            {
                LoadInsuranceTypes(sender, e);
            }

            if (LoadRisks != null)
            {
                LoadRisks(sender, e);
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            IntroForm introForm = new IntroForm();
            introForm.Show();

            base.OnClosed(e);

        }

        public event EventHandler LoadInsuranceTypes;
        public event EventHandler LoadRisks;

        public List<Insurance_Type> InsuranceTypes
        {
            set { insuranceTypesDataGridView.DataSource = value; }
        }

        public List<Risk> Risks
        {
            set { risksDataGridView.DataSource = value; }
        }

        private void addInsuranceTypeButton_Click(object sender, EventArgs e)
        {

        }

        private void editInsuranceTypeButton_Click(object sender, EventArgs e)
        {

        }

        private void removeInsuranceTypeButton_Click(object sender, EventArgs e)
        {

        }

        private void addRiskButton_Click(object sender, EventArgs e)
        {

        }

        private void editRiskButton_Click(object sender, EventArgs e)
        {

        }

        private void removeRiskButton_Click(object sender, EventArgs e)
        {

        }
    }
}
