// <copyright file="CarTest.cs">Copyright �  2012</copyright>
using System;
using System.Data.Linq;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Model
{
    /// <summary>This class contains parameterized unit tests for Car</summary>
    [PexClass(typeof(Car))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class CarTest
    {
        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        public Car Constructor()
        {
            Car target = new Car();
            return target;
            // TODO: add assertions to method CarTest.Constructor()
        }

        /// <summary>Test stub for get_Insurances()</summary>
        [PexMethod]
        public EntitySet<Insurance> InsurancesGet([PexAssumeUnderTest]Car target)
        {
            EntitySet<Insurance> result = target.Insurances;
            return result;
            // TODO: add assertions to method CarTest.InsurancesGet(Car)
        }

        /// <summary>Test stub for set_Insurances(EntitySet`1&lt;Insurance&gt;)</summary>
        [PexMethod]
        public void InsurancesSet([PexAssumeUnderTest]Car target, EntitySet<Insurance> value)
        {
            target.Insurances = value;
            // TODO: add assertions to method CarTest.InsurancesSet(Car, EntitySet`1<Insurance>)
        }

        /// <summary>Test stub for get_Person()</summary>
        [PexMethod]
        public Person PersonGet([PexAssumeUnderTest]Car target)
        {
            Person result = target.Person;
            return result;
            // TODO: add assertions to method CarTest.PersonGet(Car)
        }

        /// <summary>Test stub for set_Person(Person)</summary>
        [PexMethod]
        public void PersonSet([PexAssumeUnderTest]Car target, Person value)
        {
            target.Person = value;
            // TODO: add assertions to method CarTest.PersonSet(Car, Person)
        }

        /// <summary>Test stub for get_VIN()</summary>
        [PexMethod]
        public string VINGet([PexAssumeUnderTest]Car target)
        {
            string result = target.VIN;
            return result;
            // TODO: add assertions to method CarTest.VINGet(Car)
        }

        /// <summary>Test stub for set_VIN(String)</summary>
        [PexMethod]
        public void VINSet([PexAssumeUnderTest]Car target, string value)
        {
            target.VIN = value;
            // TODO: add assertions to method CarTest.VINSet(Car, String)
        }

        /// <summary>Test stub for get_brand()</summary>
        [PexMethod]
        public string brandGet([PexAssumeUnderTest]Car target)
        {
            string result = target.brand;
            return result;
            // TODO: add assertions to method CarTest.brandGet(Car)
        }

        /// <summary>Test stub for set_brand(String)</summary>
        [PexMethod]
        public void brandSet([PexAssumeUnderTest]Car target, string value)
        {
            target.brand = value;
            // TODO: add assertions to method CarTest.brandSet(Car, String)
        }

        /// <summary>Test stub for get_car_id()</summary>
        [PexMethod]
        public Guid car_idGet([PexAssumeUnderTest]Car target)
        {
            Guid result = target.car_id;
            return result;
            // TODO: add assertions to method CarTest.car_idGet(Car)
        }

        /// <summary>Test stub for set_car_id(Guid)</summary>
        [PexMethod]
        public void car_idSet([PexAssumeUnderTest]Car target, Guid value)
        {
            target.car_id = value;
            // TODO: add assertions to method CarTest.car_idSet(Car, Guid)
        }

        /// <summary>Test stub for get_engine()</summary>
        [PexMethod]
        public string engineGet([PexAssumeUnderTest]Car target)
        {
            string result = target.engine;
            return result;
            // TODO: add assertions to method CarTest.engineGet(Car)
        }

        /// <summary>Test stub for set_engine(String)</summary>
        [PexMethod]
        public void engineSet([PexAssumeUnderTest]Car target, string value)
        {
            target.engine = value;
            // TODO: add assertions to method CarTest.engineSet(Car, String)
        }

        /// <summary>Test stub for get_person_id()</summary>
        [PexMethod]
        public Guid person_idGet([PexAssumeUnderTest]Car target)
        {
            Guid result = target.person_id;
            return result;
            // TODO: add assertions to method CarTest.person_idGet(Car)
        }

        /// <summary>Test stub for set_person_id(Guid)</summary>
        [PexMethod]
        public void person_idSet([PexAssumeUnderTest]Car target, Guid value)
        {
            target.person_id = value;
            // TODO: add assertions to method CarTest.person_idSet(Car, Guid)
        }

        /// <summary>Test stub for get_type()</summary>
        [PexMethod]
        public string typeGet([PexAssumeUnderTest]Car target)
        {
            string result = target.type;
            return result;
            // TODO: add assertions to method CarTest.typeGet(Car)
        }

        /// <summary>Test stub for set_type(String)</summary>
        [PexMethod]
        public void typeSet([PexAssumeUnderTest]Car target, string value)
        {
            target.type = value;
            // TODO: add assertions to method CarTest.typeSet(Car, String)
        }
    }
}
