// <copyright file="HouseTest.cs">Copyright �  2012</copyright>
using System;
using System.Data.Linq;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Model
{
    /// <summary>This class contains parameterized unit tests for House</summary>
    [PexClass(typeof(House))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class HouseTest
    {
        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        public House Constructor()
        {
            House target = new House();
            return target;
            // TODO: add assertions to method HouseTest.Constructor()
        }

        /// <summary>Test stub for get_Insurances()</summary>
        [PexMethod]
        public EntitySet<Insurance> InsurancesGet([PexAssumeUnderTest]House target)
        {
            EntitySet<Insurance> result = target.Insurances;
            return result;
            // TODO: add assertions to method HouseTest.InsurancesGet(House)
        }

        /// <summary>Test stub for set_Insurances(EntitySet`1&lt;Insurance&gt;)</summary>
        [PexMethod]
        public void InsurancesSet([PexAssumeUnderTest]House target, EntitySet<Insurance> value)
        {
            target.Insurances = value;
            // TODO: add assertions to method HouseTest.InsurancesSet(House, EntitySet`1<Insurance>)
        }

        /// <summary>Test stub for get_Person()</summary>
        [PexMethod]
        public Person PersonGet([PexAssumeUnderTest]House target)
        {
            Person result = target.Person;
            return result;
            // TODO: add assertions to method HouseTest.PersonGet(House)
        }

        /// <summary>Test stub for set_Person(Person)</summary>
        [PexMethod]
        public void PersonSet([PexAssumeUnderTest]House target, Person value)
        {
            target.Person = value;
            // TODO: add assertions to method HouseTest.PersonSet(House, Person)
        }

        /// <summary>Test stub for get_address()</summary>
        [PexMethod]
        public string addressGet([PexAssumeUnderTest]House target)
        {
            string result = target.address;
            return result;
            // TODO: add assertions to method HouseTest.addressGet(House)
        }

        /// <summary>Test stub for set_address(String)</summary>
        [PexMethod]
        public void addressSet([PexAssumeUnderTest]House target, string value)
        {
            target.address = value;
            // TODO: add assertions to method HouseTest.addressSet(House, String)
        }

        /// <summary>Test stub for get_city()</summary>
        [PexMethod]
        public string cityGet([PexAssumeUnderTest]House target)
        {
            string result = target.city;
            return result;
            // TODO: add assertions to method HouseTest.cityGet(House)
        }

        /// <summary>Test stub for set_city(String)</summary>
        [PexMethod]
        public void citySet([PexAssumeUnderTest]House target, string value)
        {
            target.city = value;
            // TODO: add assertions to method HouseTest.citySet(House, String)
        }

        /// <summary>Test stub for get_description()</summary>
        [PexMethod]
        public string descriptionGet([PexAssumeUnderTest]House target)
        {
            string result = target.description;
            return result;
            // TODO: add assertions to method HouseTest.descriptionGet(House)
        }

        /// <summary>Test stub for set_description(String)</summary>
        [PexMethod]
        public void descriptionSet([PexAssumeUnderTest]House target, string value)
        {
            target.description = value;
            // TODO: add assertions to method HouseTest.descriptionSet(House, String)
        }

        /// <summary>Test stub for get_house_id()</summary>
        [PexMethod]
        public Guid house_idGet([PexAssumeUnderTest]House target)
        {
            Guid result = target.house_id;
            return result;
            // TODO: add assertions to method HouseTest.house_idGet(House)
        }

        /// <summary>Test stub for set_house_id(Guid)</summary>
        [PexMethod]
        public void house_idSet([PexAssumeUnderTest]House target, Guid value)
        {
            target.house_id = value;
            // TODO: add assertions to method HouseTest.house_idSet(House, Guid)
        }

        /// <summary>Test stub for get_nr_rooms()</summary>
        [PexMethod]
        public int nr_roomsGet([PexAssumeUnderTest]House target)
        {
            int result = target.nr_rooms;
            return result;
            // TODO: add assertions to method HouseTest.nr_roomsGet(House)
        }

        /// <summary>Test stub for set_nr_rooms(Int32)</summary>
        [PexMethod]
        public void nr_roomsSet([PexAssumeUnderTest]House target, int value)
        {
            target.nr_rooms = value;
            // TODO: add assertions to method HouseTest.nr_roomsSet(House, Int32)
        }

        /// <summary>Test stub for get_person_id()</summary>
        [PexMethod]
        public Guid person_idGet([PexAssumeUnderTest]House target)
        {
            Guid result = target.person_id;
            return result;
            // TODO: add assertions to method HouseTest.person_idGet(House)
        }

        /// <summary>Test stub for set_person_id(Guid)</summary>
        [PexMethod]
        public void person_idSet([PexAssumeUnderTest]House target, Guid value)
        {
            target.person_id = value;
            // TODO: add assertions to method HouseTest.person_idSet(House, Guid)
        }

        /// <summary>Test stub for get_post_code()</summary>
        [PexMethod]
        public string post_codeGet([PexAssumeUnderTest]House target)
        {
            string result = target.post_code;
            return result;
            // TODO: add assertions to method HouseTest.post_codeGet(House)
        }

        /// <summary>Test stub for set_post_code(String)</summary>
        [PexMethod]
        public void post_codeSet([PexAssumeUnderTest]House target, string value)
        {
            target.post_code = value;
            // TODO: add assertions to method HouseTest.post_codeSet(House, String)
        }
    }
}
