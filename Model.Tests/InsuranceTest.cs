// <copyright file="InsuranceTest.cs">Copyright �  2012</copyright>
using System;
using System.Data.Linq;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Model
{
    /// <summary>This class contains parameterized unit tests for Insurance</summary>
    [PexClass(typeof(Insurance))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class InsuranceTest
    {
        /// <summary>Test stub for get_Car()</summary>
        [PexMethod]
        public Car CarGet([PexAssumeUnderTest]Insurance target)
        {
            Car result = target.Car;
            return result;
            // TODO: add assertions to method InsuranceTest.CarGet(Insurance)
        }

        /// <summary>Test stub for set_Car(Car)</summary>
        [PexMethod]
        public void CarSet([PexAssumeUnderTest]Insurance target, Car value)
        {
            target.Car = value;
            // TODO: add assertions to method InsuranceTest.CarSet(Insurance, Car)
        }

        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        public Insurance Constructor()
        {
            Insurance target = new Insurance();
            return target;
            // TODO: add assertions to method InsuranceTest.Constructor()
        }

        /// <summary>Test stub for get_House()</summary>
        [PexMethod]
        public House HouseGet([PexAssumeUnderTest]Insurance target)
        {
            House result = target.House;
            return result;
            // TODO: add assertions to method InsuranceTest.HouseGet(Insurance)
        }

        /// <summary>Test stub for set_House(House)</summary>
        [PexMethod]
        public void HouseSet([PexAssumeUnderTest]Insurance target, House value)
        {
            target.House = value;
            // TODO: add assertions to method InsuranceTest.HouseSet(Insurance, House)
        }

        /// <summary>Test stub for get_Insurance_Type()</summary>
        [PexMethod]
        public Insurance_Type Insurance_TypeGet([PexAssumeUnderTest]Insurance target)
        {
            Insurance_Type result = target.Insurance_Type;
            return result;
            // TODO: add assertions to method InsuranceTest.Insurance_TypeGet(Insurance)
        }

        /// <summary>Test stub for set_Insurance_Type(Insurance_Type)</summary>
        [PexMethod]
        public void Insurance_TypeSet([PexAssumeUnderTest]Insurance target, Insurance_Type value)
        {
            target.Insurance_Type = value;
            // TODO: add assertions to method InsuranceTest.Insurance_TypeSet(Insurance, Insurance_Type)
        }

        /// <summary>Test stub for get_Person()</summary>
        [PexMethod]
        public Person PersonGet([PexAssumeUnderTest]Insurance target)
        {
            Person result = target.Person;
            return result;
            // TODO: add assertions to method InsuranceTest.PersonGet(Insurance)
        }

        /// <summary>Test stub for set_Person(Person)</summary>
        [PexMethod]
        public void PersonSet([PexAssumeUnderTest]Insurance target, Person value)
        {
            target.Person = value;
            // TODO: add assertions to method InsuranceTest.PersonSet(Insurance, Person)
        }

        /// <summary>Test stub for get_Rate()</summary>
        [PexMethod]
        public Rate RateGet([PexAssumeUnderTest]Insurance target)
        {
            Rate result = target.Rate;
            return result;
            // TODO: add assertions to method InsuranceTest.RateGet(Insurance)
        }

        /// <summary>Test stub for set_Rate(Rate)</summary>
        [PexMethod]
        public void RateSet([PexAssumeUnderTest]Insurance target, Rate value)
        {
            target.Rate = value;
            // TODO: add assertions to method InsuranceTest.RateSet(Insurance, Rate)
        }

        /// <summary>Test stub for get_Rates()</summary>
        [PexMethod]
        public EntitySet<Rate> RatesGet([PexAssumeUnderTest]Insurance target)
        {
            EntitySet<Rate> result = target.Rates;
            return result;
            // TODO: add assertions to method InsuranceTest.RatesGet(Insurance)
        }

        /// <summary>Test stub for set_Rates(EntitySet`1&lt;Rate&gt;)</summary>
        [PexMethod]
        public void RatesSet([PexAssumeUnderTest]Insurance target, EntitySet<Rate> value)
        {
            target.Rates = value;
            // TODO: add assertions to method InsuranceTest.RatesSet(Insurance, EntitySet`1<Rate>)
        }

        /// <summary>Test stub for get_car_id()</summary>
        [PexMethod]
        public Guid? car_idGet([PexAssumeUnderTest]Insurance target)
        {
            Guid? result = target.car_id;
            return result;
            // TODO: add assertions to method InsuranceTest.car_idGet(Insurance)
        }

        /// <summary>Test stub for set_car_id(Nullable`1&lt;Guid&gt;)</summary>
        [PexMethod]
        public void car_idSet([PexAssumeUnderTest]Insurance target, Guid? value)
        {
            target.car_id = value;
            // TODO: add assertions to method InsuranceTest.car_idSet(Insurance, Nullable`1<Guid>)
        }

        /// <summary>Test stub for get_house_id()</summary>
        [PexMethod]
        public Guid? house_idGet([PexAssumeUnderTest]Insurance target)
        {
            Guid? result = target.house_id;
            return result;
            // TODO: add assertions to method InsuranceTest.house_idGet(Insurance)
        }

        /// <summary>Test stub for set_house_id(Nullable`1&lt;Guid&gt;)</summary>
        [PexMethod]
        public void house_idSet([PexAssumeUnderTest]Insurance target, Guid? value)
        {
            target.house_id = value;
            // TODO: add assertions to method InsuranceTest.house_idSet(Insurance, Nullable`1<Guid>)
        }

        /// <summary>Test stub for get_insurance_id()</summary>
        [PexMethod]
        public Guid insurance_idGet([PexAssumeUnderTest]Insurance target)
        {
            Guid result = target.insurance_id;
            return result;
            // TODO: add assertions to method InsuranceTest.insurance_idGet(Insurance)
        }

        /// <summary>Test stub for set_insurance_id(Guid)</summary>
        [PexMethod]
        public void insurance_idSet([PexAssumeUnderTest]Insurance target, Guid value)
        {
            target.insurance_id = value;
            // TODO: add assertions to method InsuranceTest.insurance_idSet(Insurance, Guid)
        }

        /// <summary>Test stub for get_insurance_type_id()</summary>
        [PexMethod]
        public Guid insurance_type_idGet([PexAssumeUnderTest]Insurance target)
        {
            Guid result = target.insurance_type_id;
            return result;
            // TODO: add assertions to method InsuranceTest.insurance_type_idGet(Insurance)
        }

        /// <summary>Test stub for set_insurance_type_id(Guid)</summary>
        [PexMethod]
        public void insurance_type_idSet([PexAssumeUnderTest]Insurance target, Guid value)
        {
            target.insurance_type_id = value;
            // TODO: add assertions to method InsuranceTest.insurance_type_idSet(Insurance, Guid)
        }

        /// <summary>Test stub for get_last_rate_id()</summary>
        [PexMethod]
        public Guid? last_rate_idGet([PexAssumeUnderTest]Insurance target)
        {
            Guid? result = target.last_rate_id;
            return result;
            // TODO: add assertions to method InsuranceTest.last_rate_idGet(Insurance)
        }

        /// <summary>Test stub for set_last_rate_id(Nullable`1&lt;Guid&gt;)</summary>
        [PexMethod]
        public void last_rate_idSet([PexAssumeUnderTest]Insurance target, Guid? value)
        {
            target.last_rate_id = value;
            // TODO: add assertions to method InsuranceTest.last_rate_idSet(Insurance, Nullable`1<Guid>)
        }

        /// <summary>Test stub for get_last_risk_rate()</summary>
        [PexMethod]
        public float? last_risk_rateGet([PexAssumeUnderTest]Insurance target)
        {
            float? result = target.last_risk_rate;
            return result;
            // TODO: add assertions to method InsuranceTest.last_risk_rateGet(Insurance)
        }

        /// <summary>Test stub for set_last_risk_rate(Nullable`1&lt;Single&gt;)</summary>
        [PexMethod]
        public void last_risk_rateSet([PexAssumeUnderTest]Insurance target, float? value)
        {
            target.last_risk_rate = value;
            // TODO: add assertions to method InsuranceTest.last_risk_rateSet(Insurance, Nullable`1<Single>)
        }

        /// <summary>Test stub for get_person_id()</summary>
        [PexMethod]
        public Guid person_idGet([PexAssumeUnderTest]Insurance target)
        {
            Guid result = target.person_id;
            return result;
            // TODO: add assertions to method InsuranceTest.person_idGet(Insurance)
        }

        /// <summary>Test stub for set_person_id(Guid)</summary>
        [PexMethod]
        public void person_idSet([PexAssumeUnderTest]Insurance target, Guid value)
        {
            target.person_id = value;
            // TODO: add assertions to method InsuranceTest.person_idSet(Insurance, Guid)
        }
    }
}
