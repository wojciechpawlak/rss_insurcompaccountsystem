// <copyright file="RiskTest.cs">Copyright �  2012</copyright>
using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Model
{
    /// <summary>This class contains parameterized unit tests for Risk</summary>
    [PexClass(typeof(Risk))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class RiskTest
    {
        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        public Risk Constructor()
        {
            Risk target = new Risk();
            return target;
            // TODO: add assertions to method RiskTest.Constructor()
        }

        /// <summary>Test stub for get_Insurance_Type()</summary>
        [PexMethod]
        public Insurance_Type Insurance_TypeGet([PexAssumeUnderTest]Risk target)
        {
            Insurance_Type result = target.Insurance_Type;
            return result;
            // TODO: add assertions to method RiskTest.Insurance_TypeGet(Risk)
        }

        /// <summary>Test stub for set_Insurance_Type(Insurance_Type)</summary>
        [PexMethod]
        public void Insurance_TypeSet([PexAssumeUnderTest]Risk target, Insurance_Type value)
        {
            target.Insurance_Type = value;
            // TODO: add assertions to method RiskTest.Insurance_TypeSet(Risk, Insurance_Type)
        }

        /// <summary>Test stub for get_class_name()</summary>
        [PexMethod]
        public string class_nameGet([PexAssumeUnderTest]Risk target)
        {
            string result = target.class_name;
            return result;
            // TODO: add assertions to method RiskTest.class_nameGet(Risk)
        }

        /// <summary>Test stub for set_class_name(String)</summary>
        [PexMethod]
        public void class_nameSet([PexAssumeUnderTest]Risk target, string value)
        {
            target.class_name = value;
            // TODO: add assertions to method RiskTest.class_nameSet(Risk, String)
        }

        /// <summary>Test stub for get_description()</summary>
        [PexMethod]
        public string descriptionGet([PexAssumeUnderTest]Risk target)
        {
            string result = target.description;
            return result;
            // TODO: add assertions to method RiskTest.descriptionGet(Risk)
        }

        /// <summary>Test stub for set_description(String)</summary>
        [PexMethod]
        public void descriptionSet([PexAssumeUnderTest]Risk target, string value)
        {
            target.description = value;
            // TODO: add assertions to method RiskTest.descriptionSet(Risk, String)
        }

        /// <summary>Test stub for get_field()</summary>
        [PexMethod]
        public string fieldGet([PexAssumeUnderTest]Risk target)
        {
            string result = target.field;
            return result;
            // TODO: add assertions to method RiskTest.fieldGet(Risk)
        }

        /// <summary>Test stub for set_field(String)</summary>
        [PexMethod]
        public void fieldSet([PexAssumeUnderTest]Risk target, string value)
        {
            target.field = value;
            // TODO: add assertions to method RiskTest.fieldSet(Risk, String)
        }

        /// <summary>Test stub for get_insurance_type_id()</summary>
        [PexMethod]
        public Guid insurance_type_idGet([PexAssumeUnderTest]Risk target)
        {
            Guid result = target.insurance_type_id;
            return result;
            // TODO: add assertions to method RiskTest.insurance_type_idGet(Risk)
        }

        /// <summary>Test stub for set_insurance_type_id(Guid)</summary>
        [PexMethod]
        public void insurance_type_idSet([PexAssumeUnderTest]Risk target, Guid value)
        {
            target.insurance_type_id = value;
            // TODO: add assertions to method RiskTest.insurance_type_idSet(Risk, Guid)
        }

        /// <summary>Test stub for get_max()</summary>
        [PexMethod]
        public float? maxGet([PexAssumeUnderTest]Risk target)
        {
            float? result = target.max;
            return result;
            // TODO: add assertions to method RiskTest.maxGet(Risk)
        }

        /// <summary>Test stub for set_max(Nullable`1&lt;Single&gt;)</summary>
        [PexMethod]
        public void maxSet([PexAssumeUnderTest]Risk target, float? value)
        {
            target.max = value;
            // TODO: add assertions to method RiskTest.maxSet(Risk, Nullable`1<Single>)
        }

        /// <summary>Test stub for get_min()</summary>
        [PexMethod]
        public float? minGet([PexAssumeUnderTest]Risk target)
        {
            float? result = target.min;
            return result;
            // TODO: add assertions to method RiskTest.minGet(Risk)
        }

        /// <summary>Test stub for set_min(Nullable`1&lt;Single&gt;)</summary>
        [PexMethod]
        public void minSet([PexAssumeUnderTest]Risk target, float? value)
        {
            target.min = value;
            // TODO: add assertions to method RiskTest.minSet(Risk, Nullable`1<Single>)
        }

        /// <summary>Test stub for get_risk_id()</summary>
        [PexMethod]
        public Guid risk_idGet([PexAssumeUnderTest]Risk target)
        {
            Guid result = target.risk_id;
            return result;
            // TODO: add assertions to method RiskTest.risk_idGet(Risk)
        }

        /// <summary>Test stub for set_risk_id(Guid)</summary>
        [PexMethod]
        public void risk_idSet([PexAssumeUnderTest]Risk target, Guid value)
        {
            target.risk_id = value;
            // TODO: add assertions to method RiskTest.risk_idSet(Risk, Guid)
        }

        /// <summary>Test stub for get_value()</summary>
        [PexMethod]
        public int valueGet([PexAssumeUnderTest]Risk target)
        {
            int result = target.value;
            return result;
            // TODO: add assertions to method RiskTest.valueGet(Risk)
        }

        /// <summary>Test stub for set_value(Int32)</summary>
        [PexMethod]
        public void valueSet([PexAssumeUnderTest]Risk target, int value)
        {
            target.value = value;
            // TODO: add assertions to method RiskTest.valueSet(Risk, Int32)
        }
    }
}
