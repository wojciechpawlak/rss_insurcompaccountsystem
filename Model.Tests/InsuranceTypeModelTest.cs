// <copyright file="InsuranceTypeModelTest.cs">Copyright �  2012</copyright>
using System;
using System.Collections.Generic;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Model
{
    /// <summary>This class contains parameterized unit tests for InsuranceTypeModel</summary>
    [PexClass(typeof(InsuranceTypeModel))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class InsuranceTypeModelTest
    {
        /// <summary>Test stub for get_Data()</summary>
        [PexMethod]
        public PersonDataClassesDataContext DataGet([PexAssumeUnderTest]InsuranceTypeModel target)
        {
            PersonDataClassesDataContext result = target.Data;
            return result;
            // TODO: add assertions to method InsuranceTypeModelTest.DataGet(InsuranceTypeModel)
        }

        /// <summary>Test stub for LoadAllInsuranceType()</summary>
        [PexMethod]
        public List<Insurance_Type> LoadAllInsuranceType([PexAssumeUnderTest]InsuranceTypeModel target)
        {
            List<Insurance_Type> result = target.LoadAllInsuranceType();
            return result;
            // TODO: add assertions to method InsuranceTypeModelTest.LoadAllInsuranceType(InsuranceTypeModel)
        }
    }
}
