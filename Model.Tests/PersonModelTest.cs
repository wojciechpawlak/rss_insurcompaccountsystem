// <copyright file="PersonModelTest.cs">Copyright �  2012</copyright>
using System;
using System.Collections.Generic;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Model
{
    /// <summary>This class contains parameterized unit tests for PersonModel</summary>
    [PexClass(typeof(PersonModel))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class PersonModelTest
    {
        /// <summary>Test stub for get_Data()</summary>
        [PexMethod]
        public PersonDataClassesDataContext DataGet([PexAssumeUnderTest]PersonModel target)
        {
            PersonDataClassesDataContext result = target.Data;
            return result;
            // TODO: add assertions to method PersonModelTest.DataGet(PersonModel)
        }

        /// <summary>Test stub for LoadAllPersons()</summary>
        [PexMethod]
        public List<Person> LoadAllPersons([PexAssumeUnderTest]PersonModel target)
        {
            List<Person> result = target.LoadAllPersons();
            return result;
            // TODO: add assertions to method PersonModelTest.LoadAllPersons(PersonModel)
        }

        /// <summary>Test stub for create(Person)</summary>
        [PexMethod]
        public bool create([PexAssumeUnderTest]PersonModel target, Person p)
        {
            bool result = target.create(p);
            return result;
            // TODO: add assertions to method PersonModelTest.create(PersonModel, Person)
        }

        /// <summary>Test stub for find(Guid)</summary>
        [PexMethod]
        public Person find([PexAssumeUnderTest]PersonModel target, Guid id)
        {
            Person result = target.find(id);
            return result;
            // TODO: add assertions to method PersonModelTest.find(PersonModel, Guid)
        }

        /// <summary>Test stub for findByCPR(String)</summary>
        [PexMethod]
        public Person findByCPR([PexAssumeUnderTest]PersonModel target, string cpr)
        {
            Person result = target.findByCPR(cpr);
            return result;
            // TODO: add assertions to method PersonModelTest.findByCPR(PersonModel, String)
        }
    }
}
