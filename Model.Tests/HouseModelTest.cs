// <copyright file="HouseModelTest.cs">Copyright �  2012</copyright>
using System;
using System.Collections.Generic;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Model
{
    /// <summary>This class contains parameterized unit tests for HouseModel</summary>
    [PexClass(typeof(HouseModel))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class HouseModelTest
    {
        /// <summary>Test stub for get_Data()</summary>
        [PexMethod]
        public PersonDataClassesDataContext DataGet([PexAssumeUnderTest]HouseModel target)
        {
            PersonDataClassesDataContext result = target.Data;
            return result;
            // TODO: add assertions to method HouseModelTest.DataGet(HouseModel)
        }

        /// <summary>Test stub for LoadAllHouses()</summary>
        [PexMethod]
        public List<House> LoadAllHouses([PexAssumeUnderTest]HouseModel target)
        {
            List<House> result = target.LoadAllHouses();
            return result;
            // TODO: add assertions to method HouseModelTest.LoadAllHouses(HouseModel)
        }

        /// <summary>Test stub for find(Guid)</summary>
        [PexMethod]
        public House find([PexAssumeUnderTest]HouseModel target, Guid id)
        {
            House result = target.find(id);
            return result;
            // TODO: add assertions to method HouseModelTest.find(HouseModel, Guid)
        }
    }
}
