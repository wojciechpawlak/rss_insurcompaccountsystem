// <copyright file="RateTest.cs">Copyright �  2012</copyright>
using System;
using System.Data.Linq;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Model
{
    /// <summary>This class contains parameterized unit tests for Rate</summary>
    [PexClass(typeof(Rate))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class RateTest
    {
        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        public Rate Constructor()
        {
            Rate target = new Rate();
            return target;
            // TODO: add assertions to method RateTest.Constructor()
        }

        /// <summary>Test stub for get_Insurance()</summary>
        [PexMethod]
        public Insurance InsuranceGet([PexAssumeUnderTest]Rate target)
        {
            Insurance result = target.Insurance;
            return result;
            // TODO: add assertions to method RateTest.InsuranceGet(Rate)
        }

        /// <summary>Test stub for set_Insurance(Insurance)</summary>
        [PexMethod]
        public void InsuranceSet([PexAssumeUnderTest]Rate target, Insurance value)
        {
            target.Insurance = value;
            // TODO: add assertions to method RateTest.InsuranceSet(Rate, Insurance)
        }

        /// <summary>Test stub for get_Insurances()</summary>
        [PexMethod]
        public EntitySet<Insurance> InsurancesGet([PexAssumeUnderTest]Rate target)
        {
            EntitySet<Insurance> result = target.Insurances;
            return result;
            // TODO: add assertions to method RateTest.InsurancesGet(Rate)
        }

        /// <summary>Test stub for set_Insurances(EntitySet`1&lt;Insurance&gt;)</summary>
        [PexMethod]
        public void InsurancesSet([PexAssumeUnderTest]Rate target, EntitySet<Insurance> value)
        {
            target.Insurances = value;
            // TODO: add assertions to method RateTest.InsurancesSet(Rate, EntitySet`1<Insurance>)
        }

        /// <summary>Test stub for get_amount()</summary>
        [PexMethod]
        public decimal amountGet([PexAssumeUnderTest]Rate target)
        {
            decimal result = target.amount;
            return result;
            // TODO: add assertions to method RateTest.amountGet(Rate)
        }

        /// <summary>Test stub for set_amount(Decimal)</summary>
        [PexMethod]
        public void amountSet([PexAssumeUnderTest]Rate target, decimal value)
        {
            target.amount = value;
            // TODO: add assertions to method RateTest.amountSet(Rate, Decimal)
        }

        /// <summary>Test stub for get_date_paid()</summary>
        [PexMethod]
        public DateTime date_paidGet([PexAssumeUnderTest]Rate target)
        {
            DateTime result = target.date_paid;
            return result;
            // TODO: add assertions to method RateTest.date_paidGet(Rate)
        }

        /// <summary>Test stub for set_date_paid(DateTime)</summary>
        [PexMethod]
        public void date_paidSet([PexAssumeUnderTest]Rate target, DateTime value)
        {
            target.date_paid = value;
            // TODO: add assertions to method RateTest.date_paidSet(Rate, DateTime)
        }

        /// <summary>Test stub for get_expire_date()</summary>
        [PexMethod]
        public DateTime expire_dateGet([PexAssumeUnderTest]Rate target)
        {
            DateTime result = target.expire_date;
            return result;
            // TODO: add assertions to method RateTest.expire_dateGet(Rate)
        }

        /// <summary>Test stub for set_expire_date(DateTime)</summary>
        [PexMethod]
        public void expire_dateSet([PexAssumeUnderTest]Rate target, DateTime value)
        {
            target.expire_date = value;
            // TODO: add assertions to method RateTest.expire_dateSet(Rate, DateTime)
        }

        /// <summary>Test stub for get_insurance_id()</summary>
        [PexMethod]
        public Guid insurance_idGet([PexAssumeUnderTest]Rate target)
        {
            Guid result = target.insurance_id;
            return result;
            // TODO: add assertions to method RateTest.insurance_idGet(Rate)
        }

        /// <summary>Test stub for set_insurance_id(Guid)</summary>
        [PexMethod]
        public void insurance_idSet([PexAssumeUnderTest]Rate target, Guid value)
        {
            target.insurance_id = value;
            // TODO: add assertions to method RateTest.insurance_idSet(Rate, Guid)
        }

        /// <summary>Test stub for get_rate_id()</summary>
        [PexMethod]
        public Guid rate_idGet([PexAssumeUnderTest]Rate target)
        {
            Guid result = target.rate_id;
            return result;
            // TODO: add assertions to method RateTest.rate_idGet(Rate)
        }

        /// <summary>Test stub for set_rate_id(Guid)</summary>
        [PexMethod]
        public void rate_idSet([PexAssumeUnderTest]Rate target, Guid value)
        {
            target.rate_id = value;
            // TODO: add assertions to method RateTest.rate_idSet(Rate, Guid)
        }
    }
}
