// <copyright file="CarModelTest.cs">Copyright �  2012</copyright>
using System;
using System.Collections.Generic;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Model
{
    /// <summary>This class contains parameterized unit tests for CarModel</summary>
    [PexClass(typeof(CarModel))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class CarModelTest
    {
        /// <summary>Test stub for get_Data()</summary>
        [PexMethod]
        public PersonDataClassesDataContext DataGet([PexAssumeUnderTest]CarModel target)
        {
            PersonDataClassesDataContext result = target.Data;
            return result;
            // TODO: add assertions to method CarModelTest.DataGet(CarModel)
        }

        /// <summary>Test stub for LoadAllCars()</summary>
        [PexMethod]
        public List<Car> LoadAllCars([PexAssumeUnderTest]CarModel target)
        {
            List<Car> result = target.LoadAllCars();
            return result;
            // TODO: add assertions to method CarModelTest.LoadAllCars(CarModel)
        }

        /// <summary>Test stub for find(Guid)</summary>
        [PexMethod]
        public Car find([PexAssumeUnderTest]CarModel target, Guid id)
        {
            Car result = target.find(id);
            return result;
            // TODO: add assertions to method CarModelTest.find(CarModel, Guid)
        }
    }
}
