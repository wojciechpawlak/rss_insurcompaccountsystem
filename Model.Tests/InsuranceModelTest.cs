// <copyright file="InsuranceModelTest.cs">Copyright �  2012</copyright>
using System;
using System.Collections.Generic;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Model
{
    /// <summary>This class contains parameterized unit tests for InsuranceModel</summary>
    [PexClass(typeof(InsuranceModel))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class InsuranceModelTest
    {
        /// <summary>Test stub for get_Data()</summary>
        [PexMethod]
        public PersonDataClassesDataContext DataGet([PexAssumeUnderTest]InsuranceModel target)
        {
            PersonDataClassesDataContext result = target.Data;
            return result;
            // TODO: add assertions to method InsuranceModelTest.DataGet(InsuranceModel)
        }

        /// <summary>Test stub for LoadAllInsurances()</summary>
        [PexMethod]
        public List<Insurance> LoadAllInsurances([PexAssumeUnderTest]InsuranceModel target)
        {
            List<Insurance> result = target.LoadAllInsurances();
            return result;
            // TODO: add assertions to method InsuranceModelTest.LoadAllInsurances(InsuranceModel)
        }

        /// <summary>Test stub for create(Insurance)</summary>
        [PexMethod]
        public bool create([PexAssumeUnderTest]InsuranceModel target, Insurance insurance)
        {
            bool result = target.create(insurance);
            return result;
            // TODO: add assertions to method InsuranceModelTest.create(InsuranceModel, Insurance)
        }

        /// <summary>Test stub for find_by_person(Person)</summary>
        [PexMethod]
        public List<Insurance> find_by_person([PexAssumeUnderTest]InsuranceModel target, Person person)
        {
            List<Insurance> result = target.find_by_person(person);
            return result;
            // TODO: add assertions to method InsuranceModelTest.find_by_person(InsuranceModel, Person)
        }
    }
}
