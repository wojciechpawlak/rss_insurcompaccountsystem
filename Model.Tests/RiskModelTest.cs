// <copyright file="RiskModelTest.cs">Copyright �  2012</copyright>
using System;
using System.Collections.Generic;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Model
{
    /// <summary>This class contains parameterized unit tests for RiskModel</summary>
    [PexClass(typeof(RiskModel))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class RiskModelTest
    {
        /// <summary>Test stub for get_Data()</summary>
        [PexMethod]
        public PersonDataClassesDataContext DataGet([PexAssumeUnderTest]RiskModel target)
        {
            PersonDataClassesDataContext result = target.Data;
            return result;
            // TODO: add assertions to method RiskModelTest.DataGet(RiskModel)
        }

        /// <summary>Test stub for LoadAllRisks()</summary>
        [PexMethod]
        public List<Risk> LoadAllRisks([PexAssumeUnderTest]RiskModel target)
        {
            List<Risk> result = target.LoadAllRisks();

            Assert.IsTrue(result.Count > 0);
            return result;
            // TODO: add assertions to method RiskModelTest.LoadAllRisks(RiskModel)
        }

        /// <summary>Test stub for getRiskPercentage(Insurance, Risk)</summary>
        [PexMethod]
        public int getRiskPercentage(
            [PexAssumeUnderTest]RiskModel target,
            Insurance insurance,
            Risk risk
        )
        {
            int result = target.getRiskPercentage(insurance, risk);
            
            Assert.IsTrue(result == 10);
            // TODO: add assertions to method RiskModelTest.getRiskPercentage(RiskModel, Insurance, Risk)
            return result;
        }
    }
}
