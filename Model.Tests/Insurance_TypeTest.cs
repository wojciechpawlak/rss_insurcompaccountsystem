// <copyright file="Insurance_TypeTest.cs">Copyright �  2012</copyright>
using System;
using System.Data.Linq;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Model
{
    /// <summary>This class contains parameterized unit tests for Insurance_Type</summary>
    [PexClass(typeof(Insurance_Type))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class Insurance_TypeTest
    {
        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        public Insurance_Type Constructor()
        {
            Insurance_Type target = new Insurance_Type();
            return target;
            // TODO: add assertions to method Insurance_TypeTest.Constructor()
        }

        /// <summary>Test stub for get_Insurances()</summary>
        [PexMethod]
        public EntitySet<Insurance> InsurancesGet([PexAssumeUnderTest]Insurance_Type target)
        {
            EntitySet<Insurance> result = target.Insurances;
            return result;
            // TODO: add assertions to method Insurance_TypeTest.InsurancesGet(Insurance_Type)
        }

        /// <summary>Test stub for set_Insurances(EntitySet`1&lt;Insurance&gt;)</summary>
        [PexMethod]
        public void InsurancesSet(
            [PexAssumeUnderTest]Insurance_Type target,
            EntitySet<Insurance> value
        )
        {
            target.Insurances = value;
            // TODO: add assertions to method Insurance_TypeTest.InsurancesSet(Insurance_Type, EntitySet`1<Insurance>)
        }

        /// <summary>Test stub for get_Risks()</summary>
        [PexMethod]
        public EntitySet<Risk> RisksGet([PexAssumeUnderTest]Insurance_Type target)
        {
            EntitySet<Risk> result = target.Risks;
            return result;
            // TODO: add assertions to method Insurance_TypeTest.RisksGet(Insurance_Type)
        }

        /// <summary>Test stub for set_Risks(EntitySet`1&lt;Risk&gt;)</summary>
        [PexMethod]
        public void RisksSet(
            [PexAssumeUnderTest]Insurance_Type target,
            EntitySet<Risk> value
        )
        {
            target.Risks = value;
            // TODO: add assertions to method Insurance_TypeTest.RisksSet(Insurance_Type, EntitySet`1<Risk>)
        }

        /// <summary>Test stub for get_basic_rate()</summary>
        [PexMethod]
        public decimal? basic_rateGet([PexAssumeUnderTest]Insurance_Type target)
        {
            decimal? result = target.basic_rate;
            return result;
            // TODO: add assertions to method Insurance_TypeTest.basic_rateGet(Insurance_Type)
        }

        /// <summary>Test stub for set_basic_rate(Nullable`1&lt;Decimal&gt;)</summary>
        [PexMethod]
        public void basic_rateSet(
            [PexAssumeUnderTest]Insurance_Type target,
            decimal? value
        )
        {
            target.basic_rate = value;
            // TODO: add assertions to method Insurance_TypeTest.basic_rateSet(Insurance_Type, Nullable`1<Decimal>)
        }

        /// <summary>Test stub for get_description()</summary>
        [PexMethod]
        public string descriptionGet([PexAssumeUnderTest]Insurance_Type target)
        {
            string result = target.description;
            return result;
            // TODO: add assertions to method Insurance_TypeTest.descriptionGet(Insurance_Type)
        }

        /// <summary>Test stub for set_description(String)</summary>
        [PexMethod]
        public void descriptionSet([PexAssumeUnderTest]Insurance_Type target, string value)
        {
            target.description = value;
            // TODO: add assertions to method Insurance_TypeTest.descriptionSet(Insurance_Type, String)
        }

        /// <summary>Test stub for get_insurance_type_id()</summary>
        [PexMethod]
        public Guid insurance_type_idGet([PexAssumeUnderTest]Insurance_Type target)
        {
            Guid result = target.insurance_type_id;
            return result;
            // TODO: add assertions to method Insurance_TypeTest.insurance_type_idGet(Insurance_Type)
        }

        /// <summary>Test stub for set_insurance_type_id(Guid)</summary>
        [PexMethod]
        public void insurance_type_idSet([PexAssumeUnderTest]Insurance_Type target, Guid value)
        {
            target.insurance_type_id = value;
            // TODO: add assertions to method Insurance_TypeTest.insurance_type_idSet(Insurance_Type, Guid)
        }

        /// <summary>Test stub for get_name()</summary>
        [PexMethod]
        public string nameGet([PexAssumeUnderTest]Insurance_Type target)
        {
            string result = target.name;
            return result;
            // TODO: add assertions to method Insurance_TypeTest.nameGet(Insurance_Type)
        }

        /// <summary>Test stub for set_name(String)</summary>
        [PexMethod]
        public void nameSet([PexAssumeUnderTest]Insurance_Type target, string value)
        {
            target.name = value;
            // TODO: add assertions to method Insurance_TypeTest.nameSet(Insurance_Type, String)
        }
    }
}
