// <copyright file="RateModelTest.cs">Copyright �  2012</copyright>
using System;
using System.Collections.Generic;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Model
{
    /// <summary>This class contains parameterized unit tests for RateModel</summary>
    [PexClass(typeof(RateModel))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class RateModelTest
    {
        /// <summary>Test stub for get_Data()</summary>
        [PexMethod]
        public PersonDataClassesDataContext DataGet([PexAssumeUnderTest]RateModel target)
        {
            PersonDataClassesDataContext result = target.Data;
            return result;
            // TODO: add assertions to method RateModelTest.DataGet(RateModel)
        }

        /// <summary>Test stub for LoadAllRates()</summary>
        [PexMethod]
        public List<Rate> LoadAllRates([PexAssumeUnderTest]RateModel target)
        {
            List<Rate> result = target.LoadAllRates();
            return result;
            // TODO: add assertions to method RateModelTest.LoadAllRates(RateModel)
        }
    }
}
