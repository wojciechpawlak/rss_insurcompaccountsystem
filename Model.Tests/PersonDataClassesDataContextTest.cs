// <copyright file="PersonDataClassesDataContextTest.cs">Copyright �  2012</copyright>
using System;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Model
{
    /// <summary>This class contains parameterized unit tests for PersonDataClassesDataContext</summary>
    [PexClass(typeof(PersonDataClassesDataContext))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class PersonDataClassesDataContextTest
    {
        /// <summary>Test stub for get_Cars()</summary>
        [PexMethod]
        public Table<Car> CarsGet([PexAssumeUnderTest]PersonDataClassesDataContext target)
        {
            Table<Car> result = target.Cars;
            return result;
            // TODO: add assertions to method PersonDataClassesDataContextTest.CarsGet(PersonDataClassesDataContext)
        }

        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        public PersonDataClassesDataContext Constructor()
        {
            PersonDataClassesDataContext target = new PersonDataClassesDataContext();
            return target;
            // TODO: add assertions to method PersonDataClassesDataContextTest.Constructor()
        }

        /// <summary>Test stub for .ctor(String)</summary>
        [PexMethod]
        public PersonDataClassesDataContext Constructor01(string connection)
        {
            PersonDataClassesDataContext target
               = new PersonDataClassesDataContext(connection);
            return target;
            // TODO: add assertions to method PersonDataClassesDataContextTest.Constructor01(String)
        }

        /// <summary>Test stub for .ctor(IDbConnection)</summary>
        [PexMethod]
        public PersonDataClassesDataContext Constructor02(IDbConnection connection)
        {
            PersonDataClassesDataContext target
               = new PersonDataClassesDataContext(connection);
            return target;
            // TODO: add assertions to method PersonDataClassesDataContextTest.Constructor02(IDbConnection)
        }

        /// <summary>Test stub for .ctor(String, MappingSource)</summary>
        [PexMethod]
        public PersonDataClassesDataContext Constructor03(string connection, MappingSource mappingSource)
        {
            PersonDataClassesDataContext target
               = new PersonDataClassesDataContext(connection, mappingSource);
            return target;
            // TODO: add assertions to method PersonDataClassesDataContextTest.Constructor03(String, MappingSource)
        }

        /// <summary>Test stub for .ctor(IDbConnection, MappingSource)</summary>
        [PexMethod]
        public PersonDataClassesDataContext Constructor04(IDbConnection connection, MappingSource mappingSource)
        {
            PersonDataClassesDataContext target
               = new PersonDataClassesDataContext(connection, mappingSource);
            return target;
            // TODO: add assertions to method PersonDataClassesDataContextTest.Constructor04(IDbConnection, MappingSource)
        }

        /// <summary>Test stub for get_Houses()</summary>
        [PexMethod]
        public Table<House> HousesGet([PexAssumeUnderTest]PersonDataClassesDataContext target)
        {
            Table<House> result = target.Houses;
            return result;
            // TODO: add assertions to method PersonDataClassesDataContextTest.HousesGet(PersonDataClassesDataContext)
        }

        /// <summary>Test stub for get_Insurance_Types()</summary>
        [PexMethod]
        public Table<Insurance_Type> Insurance_TypesGet([PexAssumeUnderTest]PersonDataClassesDataContext target)
        {
            Table<Insurance_Type> result = target.Insurance_Types;
            return result;
            // TODO: add assertions to method PersonDataClassesDataContextTest.Insurance_TypesGet(PersonDataClassesDataContext)
        }

        /// <summary>Test stub for get_Insurances()</summary>
        [PexMethod]
        public Table<Insurance> InsurancesGet([PexAssumeUnderTest]PersonDataClassesDataContext target)
        {
            Table<Insurance> result = target.Insurances;
            return result;
            // TODO: add assertions to method PersonDataClassesDataContextTest.InsurancesGet(PersonDataClassesDataContext)
        }

        /// <summary>Test stub for get_Persons()</summary>
        [PexMethod]
        public Table<Person> PersonsGet([PexAssumeUnderTest]PersonDataClassesDataContext target)
        {
            Table<Person> result = target.Persons;
            return result;
            // TODO: add assertions to method PersonDataClassesDataContextTest.PersonsGet(PersonDataClassesDataContext)
        }

        /// <summary>Test stub for get_Rates()</summary>
        [PexMethod]
        public Table<Rate> RatesGet([PexAssumeUnderTest]PersonDataClassesDataContext target)
        {
            Table<Rate> result = target.Rates;
            return result;
            // TODO: add assertions to method PersonDataClassesDataContextTest.RatesGet(PersonDataClassesDataContext)
        }

        /// <summary>Test stub for get_Risks()</summary>
        [PexMethod]
        public Table<Risk> RisksGet([PexAssumeUnderTest]PersonDataClassesDataContext target)
        {
            Table<Risk> result = target.Risks;
            return result;
            // TODO: add assertions to method PersonDataClassesDataContextTest.RisksGet(PersonDataClassesDataContext)
        }
    }
}
