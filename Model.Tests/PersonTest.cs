// <copyright file="PersonTest.cs">Copyright �  2012</copyright>
using System;
using System.Data.Linq;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Model
{
    /// <summary>This class contains parameterized unit tests for Person</summary>
    [PexClass(typeof(Person))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class PersonTest
    {
        /// <summary>Test stub for get_Cars()</summary>
        [PexMethod]
        public EntitySet<Car> CarsGet([PexAssumeUnderTest]Person target)
        {
            EntitySet<Car> result = target.Cars;
            return result;
            // TODO: add assertions to method PersonTest.CarsGet(Person)
        }

        /// <summary>Test stub for set_Cars(EntitySet`1&lt;Car&gt;)</summary>
        [PexMethod]
        public void CarsSet([PexAssumeUnderTest]Person target, EntitySet<Car> value)
        {
            target.Cars = value;
            // TODO: add assertions to method PersonTest.CarsSet(Person, EntitySet`1<Car>)
        }

        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        public Person Constructor()
        {
            Person target = new Person();
            return target;
            // TODO: add assertions to method PersonTest.Constructor()
        }

        /// <summary>Test stub for get_Houses()</summary>
        [PexMethod]
        public EntitySet<House> HousesGet([PexAssumeUnderTest]Person target)
        {
            EntitySet<House> result = target.Houses;
            return result;
            // TODO: add assertions to method PersonTest.HousesGet(Person)
        }

        /// <summary>Test stub for set_Houses(EntitySet`1&lt;House&gt;)</summary>
        [PexMethod]
        public void HousesSet([PexAssumeUnderTest]Person target, EntitySet<House> value)
        {
            target.Houses = value;
            // TODO: add assertions to method PersonTest.HousesSet(Person, EntitySet`1<House>)
        }

        /// <summary>Test stub for get_Insurances()</summary>
        [PexMethod]
        public EntitySet<Insurance> InsurancesGet([PexAssumeUnderTest]Person target)
        {
            EntitySet<Insurance> result = target.Insurances;
            return result;
            // TODO: add assertions to method PersonTest.InsurancesGet(Person)
        }

        /// <summary>Test stub for set_Insurances(EntitySet`1&lt;Insurance&gt;)</summary>
        [PexMethod]
        public void InsurancesSet([PexAssumeUnderTest]Person target, EntitySet<Insurance> value)
        {
            target.Insurances = value;
            // TODO: add assertions to method PersonTest.InsurancesSet(Person, EntitySet`1<Insurance>)
        }

        /// <summary>Test stub for get_birth_date()</summary>
        [PexMethod]
        public DateTime birth_dateGet([PexAssumeUnderTest]Person target)
        {
            DateTime result = target.birth_date;
            return result;
            // TODO: add assertions to method PersonTest.birth_dateGet(Person)
        }

        /// <summary>Test stub for set_birth_date(DateTime)</summary>
        [PexMethod]
        public void birth_dateSet([PexAssumeUnderTest]Person target, DateTime value)
        {
            target.birth_date = value;
            // TODO: add assertions to method PersonTest.birth_dateSet(Person, DateTime)
        }

        /// <summary>Test stub for get_birth_place()</summary>
        [PexMethod]
        public string birth_placeGet([PexAssumeUnderTest]Person target)
        {
            string result = target.birth_place;
            return result;
            // TODO: add assertions to method PersonTest.birth_placeGet(Person)
        }

        /// <summary>Test stub for set_birth_place(String)</summary>
        [PexMethod]
        public void birth_placeSet([PexAssumeUnderTest]Person target, string value)
        {
            target.birth_place = value;
            // TODO: add assertions to method PersonTest.birth_placeSet(Person, String)
        }

        /// <summary>Test stub for get_cpr()</summary>
        [PexMethod]
        public string cprGet([PexAssumeUnderTest]Person target)
        {
            string result = target.cpr;
            return result;
            // TODO: add assertions to method PersonTest.cprGet(Person)
        }

        /// <summary>Test stub for set_cpr(String)</summary>
        [PexMethod]
        public void cprSet([PexAssumeUnderTest]Person target, string value)
        {
            target.cpr = value;
            // TODO: add assertions to method PersonTest.cprSet(Person, String)
        }

        /// <summary>Test stub for get_first_name()</summary>
        [PexMethod]
        public string first_nameGet([PexAssumeUnderTest]Person target)
        {
            string result = target.first_name;
            return result;
            // TODO: add assertions to method PersonTest.first_nameGet(Person)
        }

        /// <summary>Test stub for set_first_name(String)</summary>
        [PexMethod]
        public void first_nameSet([PexAssumeUnderTest]Person target, string value)
        {
            target.first_name = value;
            // TODO: add assertions to method PersonTest.first_nameSet(Person, String)
        }

        /// <summary>Test stub for get_person_id()</summary>
        [PexMethod]
        public Guid person_idGet([PexAssumeUnderTest]Person target)
        {
            Guid result = target.person_id;
            return result;
            // TODO: add assertions to method PersonTest.person_idGet(Person)
        }

        /// <summary>Test stub for set_person_id(Guid)</summary>
        [PexMethod]
        public void person_idSet([PexAssumeUnderTest]Person target, Guid value)
        {
            target.person_id = value;
            // TODO: add assertions to method PersonTest.person_idSet(Person, Guid)
        }

        /// <summary>Test stub for get_surname()</summary>
        [PexMethod]
        public string surnameGet([PexAssumeUnderTest]Person target)
        {
            string result = target.surname;
            return result;
            // TODO: add assertions to method PersonTest.surnameGet(Person)
        }

        /// <summary>Test stub for set_surname(String)</summary>
        [PexMethod]
        public void surnameSet([PexAssumeUnderTest]Person target, string value)
        {
            target.surname = value;
            // TODO: add assertions to method PersonTest.surnameSet(Person, String)
        }
    }
}
